# README #

Author: Shantanu Chaudhary
Email : shantanuchaudhary24@gmail.com

### Repository Details ###

This repository contains code for Hybrid Rendering Pipeline to be implemented in Unity. The hybrid rendering algorithm uses point cloud as well as polygonal mesh for rendering  3D objects.

* Summary of set up :
To run the code in this repo, clone this repo and import it as a project in Unity3D Editor.
[More details to be added later]

* Dependencies :
 * Unity3D version 5 or above
 * DirectX 10 or above based GPU (Uses Geometry Shaders)