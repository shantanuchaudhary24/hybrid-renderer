﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class OctreeNode
{
    /* For identifying the type of node */
    const int ROOT = -1;
    const int NODE = 0;
    const int LEAF = 1;

    #region private variables
    /* Denote the parameters of the bouding sphere enclosing the node*/
    private float _radius;
    private Vector3 _center;
    private Vector3 _normal;
    private Color _color;
    private int _nodeType;
    private List<OctreeNode> _children;

    /* We store triangle polygon in case the node is a leaf */
    private int [] _meshTriangles;

    #endregion

    #region public methods & contructors

    public float NodeRadius
    {
        get { return _radius; }
        set { this._radius = value; }
    }

    public int NodeType
    {
        get { return _nodeType; }
        set { this._nodeType = value; }
    }

    public Vector3 NodeCenter
    {
        get { return _center; }
        set { this._center = value; }
    }

    public Vector3 NodeNormal
    {
        get { return _normal; }
        set { this._normal = value; }
    }

    public Color NodeColor
    {
        get { return _color; }
        set { this._color = value; }
    }

    public List<OctreeNode> NodeChildren
    {
        get { return _children; }
        set { this._children = value; }
    }

    // Root Node Instantiation
    public OctreeNode ()
    {
        _radius = 0;
        _center = new Vector3 ( 0, 0, 0 );
        _normal = new Vector3 ( 0, 0, 0 );
        _color = new Color ( 0, 0, 0 );
        _nodeType = ROOT;
        _children = new List<OctreeNode> ();
    }

    // General Internal Node Instantiation 
    public OctreeNode ( float radius, Vector3 center, Vector3 normal, Color color )
    {
        _radius = radius;
        _center = center;
        _normal = normal;
        _color = color;
        _nodeType = NODE;
        _children = new List<OctreeNode> ();
    }

    // Leaf Node Instantiation
    public OctreeNode ( float radius, Vector3 center, Vector3 normal, Color color, int t0, int t1, int t2 )
    {
        _radius = radius;
        _center = center;
        _normal = normal;
        _color = color;
        _nodeType = LEAF;
        _meshTriangles = new int [ 3 ];
        setMeshTriangles ( t0, t1, t2 );
    }

    public bool setMeshTriangles ( int t0, int t1, int t2 )
    {
        if ( _meshTriangles == null )   // Sanity Check
            throw new Exception ( "MeshTriangleNotInstantiated" );

        if ( this._nodeType == LEAF )
        {
            _meshTriangles [ 0 ] = t0;
            _meshTriangles [ 1 ] = t1;
            _meshTriangles [ 2 ] = t2;
            return true;
        }
        else return false;
    }

    public int [] getMeshTriangles ()
    {
        return this._meshTriangles;
    }

    public void addChild ( OctreeNode node )
    {
        if ( _children == null )    // sanity check
            throw new Exception ( "ChildrenListNotInstantiated" );
        else _children.Add ( node );

    }

    public int getNumChildren ()
    {
        if ( _children == null )    // sanity check
            throw new Exception ( "ChildrenListNotInstantiated" );
        else return _children.Count;
    }
    #endregion

}
