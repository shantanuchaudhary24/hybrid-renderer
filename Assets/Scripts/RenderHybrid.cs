using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RenderHybrid : MonoBehaviour {
    #region public constructors and methods

    /* Camera Game Object*/
    public GameObject CameraObject;

    #endregion

    #region private variables

    /* File Information */
    string filename;

    /* Point Cloud Game Object*/
    GameObject gameObjectMesh;

    /* Material to be used for rendering */
    Material renderingMaterial;

    /* Camera Control Movement Script*/
    CameraMovementControl scriptObject;

    /* Point Cloud Parameters */
    float scale;
    float progress;
    float splatScale;
    bool isLoaded;
    int maxPoints;
    int polygonType; // 3 for triangle, 4 for quads
    int pointGroupNumMax;
    int meshGroupNum;
    int meshGroupNumMax;
    int meshGroupWidth;
    int numVertices;
    int numFaces;
    int meshPolygonNum;
    List<FaceInfo> listOfPolygons;
    List<VertexInfo> listOfVertices;
    Vector3 minValue;

    System.Diagnostics.Stopwatch stopwatch;

    const int MESH_TRIANGLE = 3;
    const int MESH_QUAD = 4;
    const int MAX_VERTICES = 65535;
    const int POINT_MESH = 0;
    const int POLY_MESH = 1;
    const int SPLAT_MESH = 2;

    /* Mesh Toggle Flag*/
    bool TOGGLE;
    int MESH_TYPE;

    /* GUI String*/
    string guiString;

    #endregion

    // Use this for initialization
    void Start ()
    {

        stopwatch = new System.Diagnostics.Stopwatch ();
        //path = PlayerPrefs.GetString ("fileLocation");
        filename = "D:\\test1" + ".bin";
        progress = 0;
        scale = 30f;
        splatScale = 0.25f;
        polygonType = MESH_TRIANGLE; // For triangular meshes. Change to 4 quad meshes
        isLoaded = false;
        TOGGLE = false;     // Initially the mesh will be loaded as mesh
        MESH_TYPE = POLY_MESH;
        renderingMaterial = Resources.Load ( "vertexMaterial" ) as Material;
        if ( File.Exists ( filename ) )
            loadBin ();
        //StartCoroutine ("loadPointCloudObj", filename);
        else
            Debug.Log ( "File '" + filename + "' could not be found" );

        /* Redraw object with Static Batching enabled*/
        gameObjectMesh.SetActive ( false );
        gameObjectMesh.isStatic = true;
        gameObjectMesh.SetActive ( true );

    }

    void loadBin ()
    {
        listOfVertices = new List<VertexInfo> ();
        listOfPolygons = new List<FaceInfo> ();
        if ( !stopwatch.IsRunning )
            stopwatch.Start ();
        using ( FileStream fstream = new FileStream ( filename, FileMode.Open, FileAccess.Read ) )
        {
            using ( BinaryReader bin = new BinaryReader ( fstream, System.Text.Encoding.Unicode ) )
            {
                numVertices = bin.ReadInt32 ();
                long pos = 0;
                pos += sizeof ( int );

                while ( pos < 9 * sizeof ( float ) * numVertices )
                {
                    VertexInfo vertex = new VertexInfo ();
                    float x = 0, y = 0, z = 0;
                    x = bin.ReadSingle () * scale;
                    y = bin.ReadSingle () * scale;
                    z = bin.ReadSingle () * scale;
                    vertex.Position = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Normal = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Vcolor = new Color ( x, y, z );

                    listOfVertices.Add ( vertex );

                    pos += ( sizeof ( float ) * 9 );

                }

                pos = 0;
                numFaces = bin.ReadInt32 ();
                meshPolygonNum = numFaces;
                pos += sizeof ( int );
                while ( pos < polygonType * sizeof ( int ) * numFaces )
                {
                    FaceInfo polygonFace = new FaceInfo ( polygonType );        // TODO polygonType has been hard coded 
                    int vertexIndex = 0;
                    for ( int i = 0 ; i < polygonType ; i++ )
                    {
                        vertexIndex = bin.ReadInt32 ();
                        polygonFace.SetFaceVertex ( i, vertexIndex );

                    }
                    listOfPolygons.Add ( polygonFace );
                    pos += ( sizeof ( int ) * polygonType );
                }

                Debug.Log ( "Loaded DataStructure Stats : Vertex List Size:" + listOfVertices.Count + " Polygon List Size : " + listOfPolygons.Count );
            }
            fstream.Close ();
            if ( stopwatch.IsRunning )
                stopwatch.Stop ();
        }

        // Calculate Elapsed Time
        TimeSpan ts = stopwatch.Elapsed;
        Debug.Log ( "Loading Time: " + ts.Hours + " Hours " + ts.Minutes + " Mins " + ts.Seconds + " Seconds " + ts.Milliseconds + " Milliseconds " );
        /* Construct Mesh using any one of the following methods */
        StartCoroutine (instantiatePolyMesh () );
        //StartCoroutine (instantiatePointCloudMesh ());
        //StartCoroutine (instantiateSplatMesh ());
        //StartCoroutine (instantiateParticleMesh ());

        /* Enable UI Control*/
        isLoaded = true;
        CameraObject.GetComponent<CameraMovementControl> ().isMovementActive = true;
    }

    // Update is called once per frame
    void Update ()
    {
        /*
        if ( Input.GetKeyUp ( KeyCode.T ) )
            TOGGLE = true;

        if ( TOGGLE )
        {
            if ( MESH_TYPE == POLY_MESH )
                MESH_TYPE = SPLAT_MESH; // Only one of these two statements must be active 
              //MESH_TYPE = POINT_MESH; // Uncomment to toggle to point mesh
            else
                MESH_TYPE = POLY_MESH;

            //StartCoroutine ( toggleMesh_poly_point ());
            //StartCoroutine ( toggleMesh_poly_splat_optimized () );
            toggleMesh_poly_splat_raw ();
            Debug.Log ("Toggling Mesh");
            TOGGLE = false;
        }
        */

    }


    void toggleMesh_poly_splat_raw ()
    {
        GameObject [] componentMeshes;
        if ( MESH_TYPE == SPLAT_MESH )
        {
            Debug.Log ( "COMPONENT MESHES ARE SPLAT MESH" );
            /*
            componentMeshes = GameObject.FindGameObjectsWithTag ( "PolygonMesh" );
            for (int meshComponentCount = 0 ; meshComponentCount < componentMeshes.Length ; meshComponentCount++ )
                DestroyObject (componentMeshes[meshComponentCount]);
            */
            Destroy ( gameObjectMesh );
            //StartCoroutine (instantiateSplatMesh());
        }
        else if ( MESH_TYPE == POLY_MESH )
        {
            Debug.Log ( "COMPONENT MESHES ARE POLY MESH" );
            componentMeshes = GameObject.FindGameObjectsWithTag ( "SplatMesh" );
        }
        else
        {
            Debug.Log ( "COMPONENT MESHES ARE NULL" );
            componentMeshes = null;
        }

    }


    // Toggles between polygonal mesh and mesh made of splats
    IEnumerator toggleMesh_poly_splat_optimized ()
    {

        GameObject [] componentMeshes;
        Dictionary<Vector3, Boolean> visited_vertices = new Dictionary<Vector3, bool> ();
        ;
        if (MESH_TYPE == SPLAT_MESH)
        {
            Debug.Log ( "COMPONENT MESHES ARE SPLAT MESH" );
            componentMeshes = GameObject.FindGameObjectsWithTag ( "PolygonMesh" );
        }
        else if (MESH_TYPE == POLY_MESH)
        {
            Debug.Log ( "COMPONENT MESHES ARE POLY MESH" );
            componentMeshes = GameObject.FindGameObjectsWithTag ( "SplatMesh" );
        }
        else
        {
            Debug.Log("COMPONENT MESHES ARE NULL");
            componentMeshes = null;
        }

        int togglingProgress = 1;       // Tracks progress bar fill status 

        // Enable GUI for showing progress bar
        isLoaded = false;

        //componentMeshes[0] = GameObject.CreatePrimitive ( PrimitiveType.Quad );
        //foreach ( GameObject meshComponent in componentMeshes )
        int meshComponentCount = 0;
        Debug.Log ( componentMeshes.Length );
        for (meshComponentCount = 0 ; meshComponentCount < componentMeshes.Length ; meshComponentCount++ )
        {
            if ( MESH_TYPE == SPLAT_MESH )
            {
                Mesh mesh = componentMeshes[ meshComponentCount ].GetComponent<MeshFilter> ().mesh;
                //Vector3 [] savedVertices = mesh.vertices;
                //Vector3 [] savedNormals = mesh.normals;
                //Vector2 [] savedUV = mesh.uv;
                //Color [] savedColor = mesh.colors;
                //mesh.Clear ();      // Clear the current mesh
                GameObject.Destroy ( componentMeshes [ meshComponentCount ].GetComponent<MeshFilter> () );
                GameObject.Destroy ( componentMeshes [ meshComponentCount ].GetComponent<MeshRenderer> () );


                // Instantitate Splat
                /*
                for ( int i=0 ;i<mesh.vertices.Length ;i++ )
                {
                    if ( visited_vertices!=null && !visited_vertices.ContainsKey(mesh.vertices[i]))
                    {
                        visited_vertices.Add (mesh.vertices[i],true);
                        GameObject quadSplat = GameObject.CreatePrimitive ( PrimitiveType.Quad );
                        quadSplat.GetComponent<MeshRenderer> ().material = renderingMaterial;
                        quadSplat.transform.position = mesh.vertices [ i ];
                        quadSplat.transform.rotation = Quaternion.FromToRotation ( -Vector3.forward, mesh.normals [ i ] );
                        quadSplat.transform.localScale = new Vector3 ( splatScale, splatScale, splatScale );
                        quadSplat.transform.parent = componentMeshes [ meshComponentCount ].transform;
                        quadSplat.SetActive ( false );
                        quadSplat.isStatic = true;
                        quadSplat.SetActive ( true ); 
                    }
                    else
                    {

                    }
                   
                    
                }
                */
                //componentMeshes [ meshComponentCount ].tag = "SplatMesh";
                

            }
            else if ( MESH_TYPE == POLY_MESH )
            {

            }
            else
                Debug.Log ("MESH_TYPE CASE NOT IMPLEMENTED");

           

            // Contruct mesh of point cloud

            // Update Progress Bar GUI
            progress = ( togglingProgress * 1.0f ) / ( componentMeshes.Length * 1.0f );
            int progressMax = Mathf.FloorToInt ( componentMeshes.Length / 15 );
            progressMax = progressMax > 0 ? progressMax : 1;
            if ( togglingProgress % progressMax == 0 )
            {
                guiString = togglingProgress.ToString () + " out of " + componentMeshes.Length.ToString () + " Mesh Groups toggled";
                yield return null;
            }
            togglingProgress++;

        }

        // Disable GUI for showing progress bar
        isLoaded = true;
    }


    // Toggle mesh between polygonal mesh and point cloud
    IEnumerator toggleMesh_poly_point ()
    {
        GameObject [] componentMeshes;
        if ( MESH_TYPE == POINT_MESH )
        {
            componentMeshes = GameObject.FindGameObjectsWithTag ( "PolygonMesh" );
        }
        else if ( MESH_TYPE == POLY_MESH )
        {
            componentMeshes = GameObject.FindGameObjectsWithTag ( "PointMesh" );
        }
        else
        {
            Debug.Log ( "COMPONENT MESHES ARE NULL" );
            componentMeshes = null;
        }
        int togglingProgress = 1;

        // Enable GUI for showing progress bar
        isLoaded = false;

        foreach (GameObject meshComponent in componentMeshes )
        {
            Mesh mesh = meshComponent.GetComponent<MeshFilter> ().mesh;
            Vector3 [] savedVertices = mesh.vertices;
            Vector3 [] savedNormals = mesh.normals;
            Vector2 [] savedUV = mesh.uv;
            Color [] savedColor = mesh.colors;
            int [] indices = new int [mesh.vertexCount];
            for ( int i = 0 ; i < mesh.vertexCount ; i++ )
                indices [ i ] = i;
            
            mesh.Clear ();      // Clear the current mesh

            // Contruct mesh of point cloud
            mesh.vertices = savedVertices;
            mesh.normals = savedNormals;
            mesh.colors = savedColor;
            mesh.uv = savedUV;

            if ( MESH_TYPE == POINT_MESH ) {
                mesh.SetIndices ( indices, MeshTopology.Points, 0 );
                meshComponent.tag = "PointMesh";
            }
            else if ( MESH_TYPE == POLY_MESH )
            {
                mesh.triangles = indices;
                meshComponent.tag = "PolygonMesh";
            }
            else
                Debug.Log ("MESH_TYPE CASE NOT IMPLEMENTED");

            mesh.RecalculateBounds ();

            meshComponent.GetComponent<Renderer> ().material = renderingMaterial;
            meshComponent.GetComponent<MeshFilter> ().mesh = mesh;
            gameObjectMesh.transform.parent = meshComponent.transform;

            // Update Progress Bar GUI
            progress = (togglingProgress * 1.0f) / ( componentMeshes.Length * 1.0f);
            int progressMax = Mathf.FloorToInt ( componentMeshes.Length / 15 );
            progressMax = progressMax > 0 ? progressMax : 1;
            if ( togglingProgress % progressMax == 0 )
            {
                guiString = togglingProgress.ToString () + " out of " + componentMeshes.Length.ToString () + " Mesh Groups toggled";
                yield return null;
            }
            togglingProgress++;

        }

        // Disable GUI for showing progress bar
        isLoaded = true;

    }


    IEnumerator instantiatePolyMesh ()
    {
        gameObjectMesh = new GameObject ( "polyMesh" );

        /* Instantiate Mesh here*/
        meshGroupWidth = Mathf.FloorToInt ( ( MAX_VERTICES * 1.0f ) / ( polygonType * 1.0f ) );
        meshGroupNumMax = Mathf.CeilToInt ( ( meshPolygonNum * 1.0f ) / ( meshGroupWidth * 1.0f ) );
        Debug.Log ( "meshPolygonNum: " + meshPolygonNum.ToString () + " meshGroupNumMax: " + meshGroupNumMax.ToString () + " meshGroupWidth: " + meshGroupWidth.ToString () );
        for ( meshGroupNum = 1 ; meshGroupNum < meshGroupNumMax ; meshGroupNum++ )
        {

            InstantiatePolyFace ( meshGroupNum, MAX_VERTICES, meshGroupWidth );
            progress = meshGroupNum * 1.0f / ( meshGroupNumMax ) * 1.0f;
            if ( meshGroupNum % Mathf.FloorToInt ( meshGroupNumMax / 3 ) == 0 )
            {
                guiString = meshGroupNum.ToString () + " out of " + meshGroupNumMax.ToString () + " Mesh Groups loaded";
                yield return null;
            }
        }

        int remainingPoints = ( meshPolygonNum - ( ( meshGroupNum - 1 ) * meshGroupWidth ) ) * polygonType;
        InstantiatePolyFace ( meshGroupNum, remainingPoints, Mathf.FloorToInt ( ( remainingPoints * 1.0f ) / ( polygonType * 1.0f ) ) ); // remaining point must be less than MAX_POINTS per mesh
    }

    void InstantiatePolyFace ( int meshId, int nPoints, int groupWidth )
    {
        // Create Mesh
        GameObject meshGroup = new GameObject ( "MeshPart_" + meshId );
        meshGroup.SetActive ( false );              // Deactivate Object in the scene
        meshGroup.tag = "PolygonMesh";              // Set object Tag
        meshGroup.isStatic = true;                  // Enable Static Batching
        meshGroup.AddComponent<MeshFilter> ();
        meshGroup.AddComponent<MeshRenderer> ();
        meshGroup.GetComponent<MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        Mesh mesh = new Mesh ();
        List<int> meshTriangleList = new List<int> ();
        Vector3 [] meshVertices = new Vector3 [ nPoints ];
        Vector3 [] meshNormals = new Vector3 [ nPoints ];
        Color [] meshColors = new Color [ nPoints ];
        FaceInfo tempObject = null;
        int vertexCount = 0; // Verices inserted into the mesh as well as their index
        int vertexIndex = 0;
        int iterator = 0;
        int startOfItertaion = ( 0 + ( meshId - 1 ) * meshGroupWidth );
        int limitOfIteration = ( ( meshId - 1 ) * meshGroupWidth ) + groupWidth;

        while ( vertexCount < nPoints )
        {
            for ( iterator = startOfItertaion ; iterator < limitOfIteration ; iterator++ )
            {
                tempObject = listOfPolygons [ iterator ];
                for ( int j = 0 ; j < polygonType ; j++ )
                {   // Adds vertices to point array as well as triangle array 3/4 at a time
                    if ( vertexCount > nPoints )
                        Debug.Log ( "MAX_VERTICES/LIMIT_POINTS EXCEEDED" );
                    vertexIndex = tempObject.ReturnFacesArrayVal ( j );
                    meshVertices [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Position;
                    meshNormals [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Normal;
                    meshColors [ vertexCount ] = ( Color ) listOfVertices [ vertexIndex ].Vcolor;
                    meshTriangleList.Add ( vertexCount );
                    vertexCount++;
                }
            }
        }

        /* Assign vertices, normals, colors and mesh triangles */
        mesh.vertices = meshVertices;
        mesh.normals = meshNormals;
        mesh.colors = meshColors;
        mesh.triangles = meshTriangleList.ToArray ();

        // UV Value not read from OBJ file
        mesh.uv = new Vector2 [ nPoints ];

        meshGroup.GetComponent<MeshFilter> ().mesh = mesh;
        meshGroup.GetComponent<Renderer> ().material = renderingMaterial;
        meshGroup.transform.parent = gameObjectMesh.transform;
        meshGroup.SetActive ( true );       // Activate object in the scene
        // Store Mesh
        //UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
        //UnityEditor.AssetDatabase.SaveAssets ();
        //UnityEditor.AssetDatabase.Refresh();
    }

    // Draws mesh with the help of Graphics high level API offered by Unity
    void instantiateHybridMeshOptimised()
    {
        Debug.Log ("Rendering using graphics draw calls");
        for ( meshGroupNum = 1 ; meshGroupNum < meshGroupNumMax ; meshGroupNum++ )
        {

            instantitateMeshFaceOptimised ( meshGroupNum, MAX_VERTICES, meshGroupWidth );
            progress = meshGroupNum * 1.0f / ( meshGroupNumMax ) * 1.0f;
            if ( meshGroupNum % Mathf.FloorToInt ( meshGroupNumMax / 3 ) == 0 )
            {
                guiString = meshGroupNum.ToString () + " out of " + meshGroupNumMax.ToString () + " Mesh Groups loaded";
                //yield return null;
            }
        }

        int remainingPoints = ( meshPolygonNum - ( ( meshGroupNum - 1 ) * meshGroupWidth ) ) * polygonType;
        instantitateMeshFaceOptimised ( meshGroupNum, remainingPoints, Mathf.FloorToInt ( ( remainingPoints * 1.0f ) / ( polygonType * 1.0f ) ) ); // remaining point must be less than MAX_POINTS per mesh
    }
    
    void instantitateMeshFaceOptimised ( int meshId, int nPoints, int groupWidth )
    {
        // Create Mesh
        //GameObject pointGroup = new GameObject ( "MeshPart_" + meshId );
        //pointGroup.tag = "PolygonMesh"; // Initially mesh is polygonal
        //pointGroup.AddComponent<MeshFilter> ();
        //pointGroup.AddComponent<MeshRenderer> ();

        Mesh mesh = new Mesh ();
        List<int> meshTriangleList = new List<int> ();
        Vector3 [] meshVertices = new Vector3 [ nPoints ];
        Vector3 [] meshNormals = new Vector3 [ nPoints ];
        Color [] meshColors = new Color [ nPoints ];
        FaceInfo tempObject = null;
        int vertexCount = 0; // Verices inserted into the mesh as well as their index
        int vertexIndex = 0;
        int iterator = 0;
        int startOfItertaion = ( 0 + ( meshId - 1 ) * meshGroupWidth );
        int limitOfIteration = ( ( meshId - 1 ) * meshGroupWidth ) + groupWidth;

        while ( vertexCount < nPoints )
        {
            for ( iterator = startOfItertaion ; iterator < limitOfIteration ; iterator++ )
            {
                tempObject = listOfPolygons [ iterator ];
                for ( int j = 0 ; j < polygonType ; j++ )
                {   // Adds vertices to point array as well as triangle array 3/4 at a time
                    if ( vertexCount > nPoints )
                        Debug.Log ( "MAX_VERTICES/LIMIT_POINTS EXCEEDED" );
                    vertexIndex = tempObject.ReturnFacesArrayVal ( j );
                    meshVertices [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Position;
                    meshNormals [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Normal;
                    meshColors [ vertexCount ] = ( Color ) listOfVertices [ vertexIndex ].Vcolor;
                    meshTriangleList.Add ( vertexCount );
                    vertexCount++;
                }
            }
        }

        mesh.vertices = meshVertices;
        mesh.normals = meshNormals;
        mesh.colors = meshColors;
        mesh.triangles = meshTriangleList.ToArray ();

        // UV Value not read from OBJ file
        mesh.uv = new Vector2 [ nPoints ];
        Debug.Log ("Drawn mesh");
        Graphics.DrawMeshNow (mesh, Vector3.zero, Quaternion.identity);
        //pointGroup.GetComponent<MeshFilter> ().mesh = mesh;
        //pointGroup.GetComponent<Renderer> ().material = Resources.Load ( "vertexMaterial" ) as Material;
        //pointGroup.transform.parent = gameObjectMesh.transform;

        // Store Mesh
        //UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
        //UnityEditor.AssetDatabase.SaveAssets ();
        //UnityEditor.AssetDatabase.Refresh();
    }


    void calculateMin ( Vector3 point )
    {
        if ( Math.Abs ( minValue.magnitude - 0 ) < Single.Epsilon )
            minValue = point;

        if ( point.x < minValue.x )
            minValue.x = point.x;
        if ( point.y < minValue.y )
            minValue.y = point.y;
        if ( point.z < minValue.z )
            minValue.z = point.z;
    }

    // Utility function to print Vector3
    void printVertex ( Vector3 inputVector )
    {
        Debug.Log ( "Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")" );
    }

    // Utility function to print SerialVector3
    void printVertex ( SerialVector3 serialInputVector )
    {
        Vector3 inputVector = ( Vector3 ) serialInputVector;
        Debug.Log ( "Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")" );
    }
    
    // Update GUI in each frame
    void OnGUI ()
    {
        if ( !isLoaded )
        {
            Debug.Log ( "GUI refresh" );
            GUI.BeginGroup ( new Rect ( Screen.width / 2 - 100, Screen.height / 2, 400.0f, 30 ) );
            GUI.Box ( new Rect ( 0, 0, 400.0f, 30.0f ), guiString );
            GUI.Box ( new Rect ( 0, 0, progress * 400.0f, 30 ), "" );
            GUI.EndGroup ();
        }
    }

}
