﻿using UnityEngine;

public class CameraMovementControl : MonoBehaviour
{

    Vector2 _mouseAbsolute;
    Vector2 _smoothMouse;

    public Vector2 clampInDegrees = new Vector2 ( 360, 180 );
    public CursorLockMode lockCursor;
    public Vector2 sensitivity = new Vector2 ( 2, 2 );
    public Vector2 smoothing = new Vector2 ( 3, 3 );
    public Vector2 targetDirection;
    public Vector2 targetCharacterDirection;

    public bool isMovementActive;

    void Start ()
    {

        // Camera Movement Control is disabled.
        isMovementActive = false;
        // Set target direction to the camera's initial orientation.
        targetDirection = transform.localRotation.eulerAngles;
        lockCursor = CursorLockMode.None;

    }

    void Update ()
    {
        if ( isMovementActive )
        {
            CameraMove ();
            CameraLook ();
        }
        if ( Input.GetKeyUp ( "space" ) )
        {
            if ( isMovementActive )
                isMovementActive = false;
            else
                isMovementActive = true;
        }
    }

    void CameraMove ()
    {
        float xAxisValue = Input.GetAxis ( "Horizontal" );
        float zAxisValue = Input.GetAxis ( "Vertical" );
        transform.Translate ( new Vector3 ( xAxisValue, 0.0f, zAxisValue ) );
    }

    void CameraLook ()
    {
        /* Ensure the cursor is always locked when set */
        Cursor.lockState = lockCursor;

        /* Allow the script to clamp based on a desired target value. */
        var targetOrientation = Quaternion.Euler ( targetDirection );

        /* Get raw mouse input for a cleaner reading on more sensitive mice. */
        var mouseDelta = new Vector2 ( Input.GetAxisRaw ( "Mouse X" ), Input.GetAxisRaw ( "Mouse Y" ) );

        /* Scale input against the sensitivity setting and multiply that against the smoothing value. */
        mouseDelta = Vector2.Scale ( mouseDelta, new Vector2 ( sensitivity.x * smoothing.x, sensitivity.y * smoothing.y ) );

        /*Interpolate mouse movement over time to apply smoothing delta. */
        _smoothMouse.x = Mathf.Lerp ( _smoothMouse.x, mouseDelta.x, 1f / smoothing.x );
        _smoothMouse.y = Mathf.Lerp ( _smoothMouse.y, mouseDelta.y, 1f / smoothing.y );

        /* Find the absolute mouse movement value from point zero. */
        _mouseAbsolute += _smoothMouse;

        /* Clamp and apply the local x value first, so as not to be affected by world transforms.*/
        if ( clampInDegrees.x < 360 )
            _mouseAbsolute.x = Mathf.Clamp ( _mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f );

        var xRotation = Quaternion.AngleAxis ( -_mouseAbsolute.y, targetOrientation * Vector3.right );
        transform.localRotation = xRotation;

        /* Then clamp and apply the global y value.*/
        if ( clampInDegrees.y < 360 )
            _mouseAbsolute.y = Mathf.Clamp ( _mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f );

        transform.localRotation *= targetOrientation;

        var yRotation = Quaternion.AngleAxis ( _mouseAbsolute.x, transform.InverseTransformDirection ( Vector3.up ) );
        transform.localRotation *= yRotation;

    }
}