﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SerialColor
{

    private float _red;
    private float _green;
    private float _blue;
    //	private float _alpha;

    public SerialColor ()
    {
        this._red = 1;
        this._green = 1;
        this._blue = 1;
        //		this._alpha = 1;
    }
    public SerialColor ( Color vec3 )
    {
        this._red = vec3.r;
        this._green = vec3.g;
        this._blue = vec3.b;
        //		this._alpha = 1;
    }

    public static implicit operator SerialColor ( Color col3 )
    {
        return new SerialColor ( col3 );
    }
    public static explicit operator Color ( SerialColor col3 )
    {
        return new Color ( col3._red, col3._green, col3._blue );
    }
}
