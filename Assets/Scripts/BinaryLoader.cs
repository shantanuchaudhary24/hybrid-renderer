﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BinaryLoader : MonoBehaviour
{
    #region private variables
    /* File Information */
    //string path;
    string filename;
    float scale;
    float progress;
    //	float splatScale;
    bool isLoaded;
    int maxPoints;
    int polygonType; // 3 for triangle, 4 for quadrilateral
    int pointGroupNumMax;
    int meshGroupNum;
    int meshGroupNumMax;
    int meshGroupWidth;
    int numVertices;
    int numFaces;
    int meshPolygonNum;
    List<FaceInfo> listofPolygons;
    List<VertexInfo> listOfVertices;
    Vector3 minValue;
    const int MESH_TRIANGLE = 3;
    const int MESH_QUAD = 4;
    const int MAX_VERTICES = 65535;

    /* GUI String*/
    string guiString;

    #endregion

    // Use this for initialization
    void Start ()
    {
        //path = PlayerPrefs.GetString ("fileLocation");
        filename = "D:\\test6" + ".obj";
        //filename = "E:\\test" + ".obj";
        progress = 0;
        scale = 2;
        //splatScale = 0.005f;
        polygonType = 3; // For triangular meshes. Change to 4 quad meshes
        isLoaded = false;
        if ( File.Exists ( filename ) )
            //StartCoroutine ("binPointCloudLoad", filename);
            StartCoroutine ( "binPointCloudStore", filename );
        //StartCoroutine ("loadMeshField", filename);
        else
            Debug.Log ( "File '" + filename + "' could not be found" );

    }

    IEnumerator binPointCloudStore ( string fileLocation )
    {
        // Read file
        //FileStream fs = File.OpenRead (filePath);
        //BufferedStream bs = new BufferedStream (fs);
        StreamReader sr = new StreamReader ( fileLocation );
        string [] buffer = sr.ReadLine ().Split ();
        numVertices = int.Parse ( buffer [ 1 ] );   // Read vertices here
        buffer = sr.ReadLine ().Split ();
        numFaces = int.Parse ( buffer [ 1 ] );      // Read number of faces here

        listofPolygons = new List<FaceInfo> ();
        listOfVertices = new List<VertexInfo> ();
        minValue = new Vector3 ();                  // For moving the object around origin

        Vector3 positionVector = Vector3.zero, normalVector = Vector3.zero;
        Color colorVector = Color.white;

        string fileStreamLine = sr.ReadLine ();

        for ( int i = 0 ; i < ( ( 2 * numVertices ) + numFaces ) ; i++ )
        {
            buffer = fileStreamLine.Split ();

            switch ( buffer [ 0 ] )
            {
                case "vn":
                    normalVector = new Vector3 ( float.Parse ( buffer [ 1 ] ) * scale, float.Parse ( buffer [ 2 ] ) * scale, float.Parse ( buffer [ 3 ] ) * scale );
                    //listOfNormals.Add (normalVector);
                    break;

                case "v":
                    positionVector = new Vector3 ( float.Parse ( buffer [ 1 ] ) * scale, float.Parse ( buffer [ 2 ] ) * scale, float.Parse ( buffer [ 3 ] ) * scale );
                    //listOfVertices.Add (positionVector);
                    //Color vertexColor;
                    if ( buffer.Length > 4 )
                    {
                        colorVector = new Color ( float.Parse ( buffer [ 4 ] ), float.Parse ( buffer [ 5 ] ), float.Parse ( buffer [ 6 ] ) );
                        //listOfVertexColor.Add (vertexColor);
                    }
                    else
                    {
                        colorVector = Color.white;
                        //listOfVertexColor.Add (vertexColor);
                    }

                    VertexInfo newVertex = new VertexInfo ( positionVector, normalVector, colorVector );
                    listOfVertices.Add ( newVertex );

                    /* Shift to origin */
                    calculateMin ( positionVector );

                    /* Update UI cummulatively for vertex position as well as normals*/

                    //int progressCounter = i;
                    progress = i * 1.0f / ( 2 * numVertices - 1 ) * 1.0f;
                    if ( Math.Abs ( progress % Mathf.FloorToInt ( ( 2 * numVertices ) / 20 ) - 0 ) < Single.Epsilon )
                    {
                        guiString = ( ( progress / 2 ) ).ToString () + " out of " + numVertices.ToString () + " vertices read";
                        yield return null;
                    }
                    break;

                case "f":
                    FaceInfo tempObject = new FaceInfo ( polygonType ); //Empty object that stores polygonType and corresponding vertex indices
                                                                        //facesArray [meshPolygonNum] = new FaceInfo (int.Parse (buffer [0]));
                                                                        //polygonType = int.Parse (buffer [0]);

                    for ( int j = 1 ; j < buffer.Length ; j++ )
                    {
                        string [] delimiter = new string [] { "//" }; // Delimiter in obj file for faces is "//"
                        string [] resultingString = buffer [ j ].Split ( delimiter, StringSplitOptions.None );
                        int resultingIndex = int.Parse ( resultingString [ 0 ] );
                        resultingIndex--;   // Start index at one less than what is mentioned so that 1 is saved as 0 and so on
                        tempObject.SetFaceVertex ( j - 1, resultingIndex );
                    }
                    listofPolygons.Add ( tempObject );
                    meshPolygonNum++;

                    /* Update UI */
                    progress = meshPolygonNum * 1.0f / ( numFaces - 1 ) * 1.0f;
                    if ( meshPolygonNum % Mathf.FloorToInt ( numFaces / 20 ) == 0 )
                    {
                        guiString = "Loading Mesh";
                        yield return null;
                    }

                    break;
                default:
                    Debug.Log ( "STRAY INPUT IN FILE" + buffer [ 0 ] );
                    break;
            }
            fileStreamLine = sr.ReadLine ();
        }

        /* Close file stream*/
        sr.Close ();
        //Debug.Log ("Number of Vertices :" + numVertices + " Number of Faces : " + numFaces);
        //Debug.Log ("DataStructure Stats : Vertex List Size:" + listOfVerticesPrime.Count + " Normal List Size:" + listOfVerticesPrime.Count + " Color List Size:" + listOfVerticesPrime.Count + " Polygon List Size:" + listofPolygons.Count);

        //Write binary File For vertices
        encodeData ();
        //decodeData ();

    }

    void encodeData ()
    {
        using ( FileStream fstream = new FileStream ( "D:\\test.bin", FileMode.Create, FileAccess.Write ) )
        {
            using ( BinaryWriter bin = new BinaryWriter ( fstream, System.Text.Encoding.Unicode ) )
            {

                // Write Vertex Info to file
                Debug.Log ( "NumOfVertices :" + numVertices );
                bin.Write ( numVertices );
                foreach ( VertexInfo iterator in listOfVertices )
                {
                    //for (i=0; i<numVertices; i++) {
                    // Write position
                    //VertexInfo iterator = listOfVerticesPrime [i];
                    bin.Write ( iterator.Position.x );
                    bin.Write ( iterator.Position.y );
                    bin.Write ( iterator.Position.z );

                    // Write Normal
                    bin.Write ( iterator.Normal.x );
                    bin.Write ( iterator.Normal.y );
                    bin.Write ( iterator.Normal.z );

                    // Write Color 
                    bin.Write ( iterator.Vcolor.r );
                    bin.Write ( iterator.Vcolor.g );
                    bin.Write ( iterator.Vcolor.b );

                }

                // Write Face Info to file
                Debug.Log ( "NumOfFaces :" + numFaces );
                bin.Write ( numFaces );
                foreach ( FaceInfo iterator in listofPolygons )
                {
                    for ( int i = 0 ; i < polygonType ; i++ )
                    {
                        bin.Write ( iterator.ReturnFacesArrayVal ( i ) );
                    }

                }
            }
            fstream.Close ();
        }
        Debug.Log ( "Encoding Complete!" );
    }

    void decodeData ()
    {

        List<VertexInfo> listOfVerticesPrime = new List<VertexInfo> ();
        List<FaceInfo> listOfPolygonsPrime = new List<FaceInfo> ();
        long pos = 0;
        using ( FileStream fstream = new FileStream ( "D:\\test.bin", FileMode.Open, FileAccess.Read ) )
        {
            using ( BinaryReader bin = new BinaryReader ( fstream, System.Text.Encoding.Unicode ) )
            {
                int numOfVertices = 0, numOfFaces = 0;
                numOfVertices = bin.ReadInt32 ();
                Debug.Log ( numOfVertices );
                pos += sizeof ( int );

                while ( pos < 9 * sizeof ( float ) * numOfVertices )
                {
                    VertexInfo vertex = new VertexInfo ();
                    float x = 0, y = 0, z = 0;
                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Position = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Normal = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Vcolor = new Color ( x, y, z );

                    listOfVerticesPrime.Add ( vertex );

                    pos += ( sizeof ( float ) * 9 );

                }

                pos = 0;
                numOfFaces = bin.ReadInt32 ();
                Debug.Log ( numOfFaces );
                pos += sizeof ( int );
                while ( pos < polygonType * sizeof ( int ) * numOfFaces )
                {
                    FaceInfo polygonFace = new FaceInfo ( polygonType );        // TODO polygonType has been hard coded 
                    int vertexIndex = 0;
                    for ( int i = 0 ; i < polygonType ; i++ )
                    {
                        vertexIndex = bin.ReadInt32 ();
                        polygonFace.SetFaceVertex ( i, vertexIndex );

                    }
                    listOfPolygonsPrime.Add ( polygonFace );
                    pos += ( sizeof ( int ) * polygonType );
                }

                Debug.Log ( "DataStructure Stats : Vertex List Size:" + listOfVertices.Count + " Normal List Size:" + listOfVertices.Count + " Color List Size:" + listOfVertices.Count );
                Debug.Log ( "Prime Datastructure Stats : Vertices : " + listOfVerticesPrime.Count + " Faces : " + listOfPolygonsPrime.Count );
            }
            fstream.Close ();
        }
    }

    void printList ( List<VertexInfo> inputList )
    {
        foreach ( VertexInfo iterator in inputList )
        {
            printVertex ( iterator.Position );
        }
    }

    void calculateMin ( Vector3 point )
    {
        if ( Math.Abs ( minValue.magnitude - 0 ) < Single.Epsilon )
            minValue = point;

        if ( point.x < minValue.x )
            minValue.x = point.x;
        if ( point.y < minValue.y )
            minValue.y = point.y;
        if ( point.z < minValue.z )
            minValue.z = point.z;
    }

    void printVertex ( Vector3 inputVector )
    {
        Debug.Log ( "Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")" );
    }

    void OnGUI ()
    {
        if ( !isLoaded )
        {
            GUI.BeginGroup ( new Rect ( Screen.width / 2 - 100, Screen.height / 2, 400.0f, 30 ) );
            GUI.Box ( new Rect ( 0, 0, 400.0f, 30.0f ), guiString );
            GUI.Box ( new Rect ( 0, 0, progress * 400.0f, 30 ), "" );
            GUI.EndGroup ();
        }
    }
}
