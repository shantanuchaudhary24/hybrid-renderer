﻿using UnityEngine;

public class FaceInfo
{
    private int _numVertices;
    private int [] _faceVerticesArray;

    public int NumVertices
    {
        get { return _numVertices; }
    }

    public FaceInfo ( int x )
    {
        this._numVertices = x;
        this._faceVerticesArray = new int [ _numVertices ];
    }

    public void SetFaceVertex ( int index, int value )
    {
        if ( index < 0 || index >= _numVertices || value < 0 )
        {
            Debug.Log ( "INVALID VALUE FOR FACE VERTEX" );
            Application.Quit ();
        }
        _faceVerticesArray [ index ] = value;
    }

    public int ReturnFacesArrayVal ( int index )
    {
        return _faceVerticesArray [ index ];
    }

    public string PrintVertices ()
    {
        string outputVertices = "Vertices at Index: ";
        for ( int i = 0 ; i < _numVertices ; i++ )
        {
            outputVertices += _faceVerticesArray [ i ].ToString () + " ";
        }
        return outputVertices;
    }

}