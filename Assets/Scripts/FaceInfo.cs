using System;
using UnityEngine;
using System.Collections;
using System.IO;

public class FaceInfo
{
	private int numVertices;
	private int[] faceVerticesArray;
	public FaceInfo (int x)
	{
		numVertices = x;
		faceVerticesArray = new int[numVertices];
	}
	
	public void setFaceVertex (int index, int value)
	{
		if (index < 0 || index >= numVertices || value < 0) {
			Debug.Log ("INVALID VALUE FOR FACE VERTEX");
			Application.Quit ();
		}
		faceVerticesArray [index] = value;
	}
	
	public int returnNumVertex ()
	{
		return numVertices;
	}
	
	public int returnFacesArrayVal (int index)
	{
		return faceVerticesArray [index];
	}
	
	public string printVertices ()
	{
		string outputVertices = "Vertices at Index: ";
		//string vertexvalue = "";
		for (int i = 0; i < numVertices; i++) {
			//	vertexvalue = faceVerticesArray [i].ToString ();
			outputVertices += faceVerticesArray [i].ToString () + " ";
		}
		return outputVertices;
	}
	
}