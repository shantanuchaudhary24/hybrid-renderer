﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class SerialVector3
{
	
	private float x;
	private float y;
	private float z;
	
	public SerialVector3 ()
	{
	}
	public SerialVector3 (Vector3 vec3)
	{
		this.x = vec3.x;
		this.y = vec3.y;
		this.z = vec3.z;
	}
	
	public static implicit operator SerialVector3 (Vector3 vec3)
	{
		return new SerialVector3 (vec3);
	}
	public static explicit operator Vector3 (SerialVector3 wb_vec3)
	{
		return new Vector3 (wb_vec3.x, wb_vec3.y, wb_vec3.z);
	}
}

public class RenderPointCloud : MonoBehaviour
{
	/* Camera Game Object*/
	public GameObject cameraObject;
	
	/* File Information */
	string path;
	string filename;
	
	/* Point Cloud Game Object*/
	GameObject pointCloudObject;
	
	/* Point Cloud Parameters */
	float scale;
	float progress;
	float splatScale;
	bool invertYZ;
	bool isLoaded;
	int maxPoints;
	int polygonType; // 3 for triangle, 4 for quadrilateral
	int pointGroupNumMax;
	int meshGroupNum;
	int meshGroupNumMax;
	int meshGroupWidth;
	int numVertices;
	int numFaces;
	int meshPolygonNum;
	List<SerialVector3> listOfVertices;
	List<SerialVector3> listOfNormals;
	List<Color> listOfVertexColor;
	List<FaceInfo> listofPolygons;
	Vector3 minValue;
	
	const int MESH_TRIANGLE = 3;
	const int MESH_QUAD = 4;
	const int MAX_VERTICES = 65535;

	/* Debug File Location */
	string debugFileLocation = "D:\\Shantanu\\Work Unity\\Projects\\test";
	/* GUI String*/
	string guiString;
	
	// Use this for initialization
	void Start ()
	{
		
		path = PlayerPrefs.GetString ("fileLocation");
		filename = path + ".obj";
		//filename = "E:\\test" + ".obj";
		progress = 0;
		scale = 2;
		splatScale = 0.005f;
		polygonType = 3; // For triangular meshes. Change to 4 quad meshes
		invertYZ = false;
		isLoaded = false;
		if (File.Exists (filename)) 
			//StartCoroutine ("binPointCloudLoad", filename);
			StartCoroutine ("loadPointCloud", filename);
			//StartCoroutine ("loadMeshField", filename);
		else 
			Debug.Log ("File '" + filename + "' could not be found"); 
			
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		
	}
	/*
	IEnumerator binPointCloudStore (string fileLocation)
	{
		encodePointCloud (fileLocation);
		return null;
	}
	
	IEnumerator binPointCloudLoad ()
	{
		decodePointCloud ();
		instantiatePointCloudMesh ();
		return null;
		
	}
	
	void decodePointCloud ()
	{
		//List<salesman> salesmanList = new List<salesman> ();
		string dir = @"c:\";
		string serializationFile = Path.Combine (dir, "info.bin");
		
		//deserialize
		using (Stream stream = File.Open(serializationFile, FileMode.Open)) {
			var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter ();
			
			listOfVertices = (List<SerialVector3>)bformatter.Deserialize (stream);
		}
	
	
		string dir = @"C:\";
		string serializationFile = Path.Combine (dir, "info.bin");
		//serialize
		using (Stream stream = File.Open(serializationFile, FileMode.Create)) {
			var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter ();
			bformatter.Serialize (stream, listOfVertices);
		}
	}
	*/
	
	IEnumerator loadPointCloud (string filePath)
	{
		// Read file
		//FileStream fs = File.OpenRead (filePath);
		//BufferedStream bs = new BufferedStream (fs);
		StreamReader sr = new StreamReader (filePath);
		string[] buffer = sr.ReadLine ().Split ();
		numVertices = int.Parse (buffer [1]);	// Read vertices here
		buffer = sr.ReadLine ().Split ();
		numFaces = int.Parse (buffer [1]);		// Read number of faces here
		
		listOfVertices = new List<SerialVector3> ();		// Stores vertex information
		listOfNormals = new List<SerialVector3> ();		// Stores vertex normals
		listOfVertexColor = new List<Color> ();		// Stores vertex color
		listofPolygons = new List<FaceInfo> ();
		minValue = new Vector3 ();					// For moving the object around origin
				
		string fileStreamLine = sr.ReadLine ();
		//while(fileStreamLine!=null){
		for (int i=0; i< ((2*numVertices)+numFaces); i++) {
			buffer = fileStreamLine.Split ();
			
			switch (buffer [0]) {
			case "vn":
				Vector3 normalVector = new Vector3 (float.Parse (buffer [1]) * scale, float.Parse (buffer [2]) * scale, float.Parse (buffer [3]) * scale);
				listOfNormals.Add (normalVector);
				break;
				
			case "v":
				Vector3 positionVector = new Vector3 (float.Parse (buffer [1]) * scale, float.Parse (buffer [2]) * scale, float.Parse (buffer [3]) * scale);
				listOfVertices.Add (positionVector);
				Color vertexColor;
				if (buffer.Length > 4) {
					vertexColor = new Color (float.Parse (buffer [4]), float.Parse (buffer [5]), float.Parse (buffer [6]));
					listOfVertexColor.Add (vertexColor);
				} else {
					vertexColor = Color.white;
					listOfVertexColor.Add (vertexColor);
				}
				
					
				/* Shift to origin */
				calculateMin (positionVector);
					
				/* Update UI cummulatively for vertex position as well as normals*/
		
				//int progressCounter = i;
				progress = i * 1.0f / (2 * numVertices - 1) * 1.0f;
				if (progress % Mathf.FloorToInt ((2 * numVertices) / 20) == 0) {
					guiString = ((progress / 2)).ToString () + " out of " + numVertices.ToString () + " vertices read";
					yield return null;
				}
				break;

			case "f":
				FaceInfo tempObject = new FaceInfo (polygonType);	//Empty object that stores polygonType and corresponding vertex indices
				//facesArray [meshPolygonNum] = new FaceInfo (int.Parse (buffer [0]));
				//polygonType = int.Parse (buffer [0]);

				for (int j = 1; j < buffer.Length; j++) {
					string[] delimiter = new string[]{"//"}; // Delimiter in obj file for faces is "//"
					string[] resultingString = buffer [j].Split (delimiter, StringSplitOptions.None);
					int resultingIndex = int.Parse (resultingString [0]);
					resultingIndex--;	// Start index at one less than what is mentioned so that 1 is saved as 0 and so on
					tempObject.setFaceVertex (j - 1, resultingIndex);
				}
				listofPolygons.Add (tempObject);
				meshPolygonNum++;
				
				/* Update UI */
				progress = meshPolygonNum * 1.0f / (numFaces - 1) * 1.0f;
				if (meshPolygonNum % Mathf.FloorToInt (numFaces / 20) == 0) {
					guiString = "Loading Mesh";
					yield return null;
				}

				break;
			default:
				Debug.LogError ("STRAY INPUT IN FILE");
				break;
			}
			fileStreamLine = sr.ReadLine ();		
		}
		
		/* Close file stream*/		
		sr.Close ();
		Debug.Log ("Number of Vertices :" + numVertices + " Number of Faces : " + numFaces);
		Debug.Log ("DataStructure Stats : Vertex List Size:" + listOfVertices.Count + " Normal List Size:" + listOfNormals.Count + " Color List Size:" + listOfVertexColor.Count + " Polygon List Size:" + listofPolygons.Count);

		/* Construct Point Cloud Mesh */
		//StartCoroutine (instantiatePointCloudMesh ());
		//StartCoroutine (instantiateSplatMesh ());
		//StartCoroutine (instantiatePolyMesh ());
		//StartCoroutine (instantiateParticleMesh ());

		/* Enable UI Control*/
		isLoaded = true;
		CameraMovementControl scriptObject = cameraObject.GetComponent<CameraMovementControl> ();
		scriptObject.isMovementActive = true;
	}
	
	IEnumerator instantiateParticleMesh ()
	{
		/*
			To run this method, insert a gameObject into the scene named as 
			PointCloudObject. Now attach this script to that game object and
			then run the load scene. This will render the point cloud as a 
			system of particles.
		 */

		this.gameObject.GetComponent<ParticleSystem> ().loop = false;
		this.gameObject.GetComponent<ParticleSystem> ().maxParticles = 1000000;
		this.gameObject.GetComponent<ParticleSystem> ().playOnAwake = false;
		this.gameObject.GetComponent<ParticleSystem> ().startSpeed = 0;
		this.gameObject.GetComponent<ParticleSystem> ().startSize = 0.01f;
		this.gameObject.GetComponent<ParticleSystem> ().enableEmission = false;
		this.gameObject.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().receiveShadows = true;
		this.gameObject.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

		ParticleSystem.Particle[] particleCloud;
		particleCloud = new ParticleSystem.Particle[numVertices];
		for (int i=0; i<numVertices; i++) {
			particleCloud [i].position = (Vector3)listOfVertices [i];
			particleCloud [i].color = listOfVertexColor [i];
			particleCloud [i].angularVelocity = 0;
			particleCloud [i].size = 0.01f;
			particleCloud [i].lifetime = 10000;
			progress = i * 1.0f / (2 * numVertices - 1) * 1.0f;
			if (progress % Mathf.FloorToInt ((2 * numVertices) / 20) == 0) {
				guiString = ((progress / 2)).ToString () + " out of " + numVertices.ToString () + " vertices read";
				yield return null;
			}
		}

		this.gameObject.GetComponent<ParticleSystem> ().SetParticles (particleCloud, numVertices);
		this.gameObject.GetComponent<ParticleSystem> ().enableEmission = true;

	}

	IEnumerator instantiatePolyMesh ()
	{
		pointCloudObject = new GameObject ("polyMesh");
		/* Instantiate Mesh here*/
		meshGroupWidth = Mathf.FloorToInt ((MAX_VERTICES * 1.0f) / (polygonType * 1.0f));
		meshGroupNumMax = Mathf.CeilToInt ((meshPolygonNum * 1.0f) / (meshGroupWidth * 1.0f));
		Debug.Log ("meshPolygonNum: " + meshPolygonNum.ToString () + " meshGroupNumMax: " + meshGroupNumMax.ToString () + " meshGroupWidth: " + meshGroupWidth.ToString ());
		for (meshGroupNum = 1; meshGroupNum < meshGroupNumMax; meshGroupNum++) {
			
			InstantiatePolyFace (meshGroupNum, MAX_VERTICES, meshGroupWidth);
			progress = meshGroupNum * 1.0f / (meshGroupNumMax) * 1.0f;
			if (meshGroupNum % Mathf.FloorToInt (meshGroupNumMax / 3) == 0) {
				guiString = meshGroupNum.ToString () + " out of " + meshGroupNumMax.ToString () + " Mesh Groups loaded";
				yield return null;
			}
		}
		
		int remainingPoints = (meshPolygonNum - ((meshGroupNum - 1) * meshGroupWidth)) * polygonType;
		InstantiatePolyFace (meshGroupNum, remainingPoints, Mathf.FloorToInt ((remainingPoints * 1.0f) / (polygonType * 1.0f))); // remaining point must be less than MAX_POINTS per mesh
	}

	void InstantiatePolyFace (int meshId, int nPoints, int groupWidth)
	{
		// Create Mesh
		GameObject pointGroup = new GameObject ("MeshPart_" + meshId);
		pointGroup.AddComponent<MeshFilter> ();
		pointGroup.AddComponent<MeshRenderer> ();

		Mesh mesh = new Mesh ();
		Debug.Log ("Limit Points Value:" + nPoints);
		List<int> meshTriangleList = new List<int> ();
		Vector3[] meshVertices = new Vector3[nPoints];
		Vector3[] meshNormals = new Vector3[nPoints];
		Color[] meshColors = new Color[nPoints];
		FaceInfo tempObject = null;
		int vertexCount = 0; // Verices inserted into the mesh as well as their index
		int vertexIndex = 0;
		int iterator = 0;
		int startOfItertaion = (0 + (meshId - 1) * meshGroupWidth);
		int limitOfIteration = ((meshId - 1) * meshGroupWidth) + groupWidth;
		
		while (vertexCount < nPoints) {
			for (iterator = startOfItertaion; iterator < limitOfIteration; iterator++) {
				tempObject = listofPolygons [iterator];
				for (int j = 0; j < polygonType; j++) {	// Adds vertices to point array as well as triangle array 3/4 at a time
					if (vertexCount > nPoints)
						Debug.Log ("MAX_VERTICES/LIMIT_POINTS EXCEEDED");
					vertexIndex = tempObject.returnFacesArrayVal (j);
					meshVertices [vertexCount] = (Vector3)listOfVertices [vertexIndex];
					meshNormals [vertexCount] = (Vector3)listOfNormals [vertexIndex];
					meshColors [vertexCount] = listOfVertexColor [vertexIndex];
					meshTriangleList.Add (vertexCount);
					vertexCount++;
				}	
			}
		}
		
		mesh.vertices = meshVertices;
		mesh.normals = meshNormals;
		mesh.colors = meshColors;
		mesh.triangles = meshTriangleList.ToArray ();
		
		// UV Value not read from OBJ file
		mesh.uv = new Vector2[nPoints];

		pointGroup.GetComponent<MeshFilter> ().mesh = mesh;
		pointGroup.GetComponent<Renderer> ().material = Resources.Load ("vertexMaterial") as Material;
		pointGroup.transform.parent = pointCloudObject.transform;
		
		// Store Mesh
		//UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
		//UnityEditor.AssetDatabase.SaveAssets ();
		//UnityEditor.AssetDatabase.Refresh();
	}

	IEnumerator instantiateSplatMesh ()
	{
		int index = 0, maxIndex = listOfVertices.Count;
		pointCloudObject = new GameObject ("SplatMesh");
		SerialVector3 positionVector, normalVector;
		for (index = 0; index < maxIndex; index++) {
			positionVector = listOfVertices [index];
			normalVector = listOfNormals [index];
			InstantiateSplat (positionVector, normalVector, index);

			progress = index * 1.0f / (numVertices - 1) * 1.0f;
			if (index % Mathf.FloorToInt (numVertices / 20) == 0) {
				guiString = index.ToString () + " out of " + maxIndex.ToString () + " splats instantiated";
				yield return null;
			}
		}

		//Store PointCloud
		//UnityEditor.PrefabUtility.CreatePrefab ("Assets/Resources/pointCloudMesh" + ".prefab", pointCloudObject);

	}

	void  InstantiateSplat (SerialVector3 posElement, SerialVector3 normElement, int index)
	{
		Vector3 normal = (Vector3)(normElement);
		if (normal.magnitude == 0)
			return;	// If normal at that vertex position is zero then don't instantiate splat
		GameObject quadSplat = GameObject.CreatePrimitive (PrimitiveType.Quad);
		quadSplat.GetComponent<MeshRenderer> ().material = Resources.Load ("vertexMaterialStandard") as Material;
		quadSplat.transform.position = (Vector3)(posElement);
		quadSplat.transform.rotation = Quaternion.FromToRotation (-Vector3.forward, normal);
		quadSplat.transform.localScale = new Vector3 (splatScale, splatScale, splatScale);
		quadSplat.transform.parent = pointCloudObject.transform;
		
	}

	IEnumerator instantiatePointCloudMesh ()
	{
		/* Logic for construction of point cloud */
		pointGroupNumMax = Mathf.CeilToInt (numVertices * 1.0f / MAX_VERTICES * 1.0f);	// Number of point groups each made up of 65535 Vertices
		pointCloudObject = new GameObject ("PoinCloudMesh");
		Debug.Log ("Number of Point Groups: " + pointGroupNumMax);
		
		
		for (int i = 0; i < pointGroupNumMax-1; i ++) {
			InstantiatePointMesh (i, MAX_VERTICES);
			if (i % 10 == 0) {
				guiString = i.ToString () + " out of " + pointGroupNumMax.ToString () + " PointGroups loaded";
				yield return null;
			}
		}
		
		/* Last point group has been instantiated here */
		InstantiatePointMesh (pointGroupNumMax - 1, numVertices - (pointGroupNumMax - 1) * MAX_VERTICES);

		//Store PointCloud
		//UnityEditor.PrefabUtility.CreatePrefab ("Assets/Resources/pointCloudMesh" + ".prefab", pointCloudObject);

		isLoaded = true;
		CameraMovementControl scriptObject = cameraObject.GetComponent<CameraMovementControl> ();
		scriptObject.isMovementActive = true;
		
	}
	
	void InstantiatePointMesh (int meshId, int nPoints)
	{
		// Create Mesh
		GameObject pointGroup = new GameObject ("PointCloudMesh_" + meshId);
		pointGroup.AddComponent<MeshFilter> ();
		pointGroup.AddComponent<MeshRenderer> ();
		pointGroup.GetComponent<Renderer> ().material = Resources.Load ("vertexMaterial") as Material;

		Mesh mesh = new Mesh ();	
		Vector3[] vertexPos = new Vector3[nPoints]; 
		Vector3[] vertexNorm = new Vector3[nPoints];
		int[] indicies = new int[nPoints];
		Color[] vertexColor = new Color[nPoints];
		
		for (int i=0; i<nPoints; ++i) {
			vertexPos [i] = (Vector3)(listOfVertices [meshId * MAX_VERTICES + i]) - minValue;
			indicies [i] = i;
			vertexNorm [i] = (Vector3)(listOfNormals [meshId * MAX_VERTICES + i]);
			vertexColor [i] = listOfVertexColor [meshId * MAX_VERTICES + i];
		}
		
		mesh.vertices = vertexPos;
		mesh.colors = vertexColor;
		mesh.SetIndices (indicies, MeshTopology.Points, 0);
		mesh.uv = new Vector2[nPoints];
		mesh.normals = vertexNorm;
		mesh.RecalculateBounds ();

		pointGroup.GetComponent<MeshFilter> ().mesh = mesh;
		pointGroup.transform.parent = pointCloudObject.transform;

		// Store Mesh
		//UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + meshInd + ".asset");
		//UnityEditor.AssetDatabase.SaveAssets ();
		//UnityEditor.AssetDatabase.Refresh();
	}
	
	void calculateMin (Vector3 point)
	{
		if (minValue.magnitude == 0)
			minValue = point;
		
		if (point.x < minValue.x)
			minValue.x = point.x;
		if (point.y < minValue.y)
			minValue.y = point.y;
		if (point.z < minValue.z)
			minValue.z = point.z;
	}
	
	void printVertex (Vector3 inputVector)
	{
		Debug.Log ("Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")");	
	}
	
	void printVertex (SerialVector3 serialInputVector)
	{
		Vector3 inputVector = (Vector3)serialInputVector;
		Debug.Log ("Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")");
	}
				
	void OnGUI ()
	{
		if (!isLoaded) {
			GUI.BeginGroup (new Rect (Screen.width / 2 - 100, Screen.height / 2, 400.0f, 30)); 
			GUI.Box (new Rect (0, 0, 400.0f, 30.0f), guiString);
			GUI.Box (new Rect (0, 0, progress * 400.0f, 30), "");
			GUI.EndGroup ();		 
		}
	} 
}
