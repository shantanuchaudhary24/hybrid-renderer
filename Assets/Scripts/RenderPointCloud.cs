using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class RenderPointCloud : MonoBehaviour
{

    /* For identifying the type of node */
    const int ROOT = -1;
    const int NODE = 0;
    const int LEAF = 1;

    #region public constructors and methods

    /* Camera Game Object*/
    public GameObject CameraObject;

    #endregion

    #region private variables

    /* File Information */
    string filename;

    /* Point Cloud Game Object*/
    GameObject meshGameObject;

    /* Point Cloud Parameters */
    float scale;
    float progress;
    float splatScale;
    bool isLoaded;
    int maxPoints;
    int polygonType; // 3 for triangle, 4 for quads
    int pointGroupNumMax;
    int meshGroupNum;
    int meshGroupNumMax;
    int meshGroupWidth;
    int numVertices;
    int numFaces;
    int meshPolygonNum;
    List<FaceInfo> listOfPolygons;
    List<VertexInfo> listOfVertices;
    Vector3 minValue;

    /* Stopwatch keeper */
    System.Diagnostics.Stopwatch stopwatch;

    /* Const parameters for mesh properties*/
    const int MESH_TRIANGLE = 3;
    const int MESH_QUAD = 4;
    const int MAX_VERTICES = 65535;

    /* Material to be used for rendering */
    Material renderingMaterial;

    /* GUI String*/
    string guiString;

    #endregion

    // Use this for initialization
    void Start ()
    {

        stopwatch = new System.Diagnostics.Stopwatch ();
        
        //path = PlayerPrefs.GetString ("fileLocation");
        filename = "D:\\test1" + ".bin";
        progress = 0;
        scale = 30f;//3 for point cloud
        splatScale = 0.25f;
        polygonType = 3; // For triangular meshes. Change to 4 quad meshes
        isLoaded = false;
        renderingMaterial = Resources.Load ( "vertexMaterial" ) as Material;
        if ( File.Exists ( filename ) )
            loadBin ();
        //StartCoroutine ("loadPointCloudObj", filename);
        else
            Debug.Log ( "File '" + filename + "' could not be found" );
        // Redraw gameObject with static batching
        meshGameObject.SetActive ( false );
        meshGameObject.isStatic = true;
        meshGameObject.SetActive ( true );

    }

    // Update is called once per frame
    void Update ()
    {

    }

    void loadBin ()
    {
        listOfVertices = new List<VertexInfo> ();
        listOfPolygons = new List<FaceInfo> ();
        if ( !stopwatch.IsRunning )
            stopwatch.Start ();
        using ( FileStream fstream = new FileStream ( filename, FileMode.Open, FileAccess.Read ) )
        {
            using ( BinaryReader bin = new BinaryReader ( fstream, System.Text.Encoding.Unicode ) )
            {
                numVertices = bin.ReadInt32 ();
                long pos = 0;
                pos += sizeof ( int );

                while ( pos < 9 * sizeof ( float ) * numVertices )
                {
                    VertexInfo vertex = new VertexInfo ();
                    float x = 0, y = 0, z = 0;
                    x = bin.ReadSingle ()* scale;
                    y = bin.ReadSingle () * scale;
                    z = bin.ReadSingle () * scale;
                    vertex.Position = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Normal = new Vector3 ( x, y, z );

                    x = bin.ReadSingle ();
                    y = bin.ReadSingle ();
                    z = bin.ReadSingle ();
                    vertex.Vcolor = new Color ( x, y, z );

                    listOfVertices.Add ( vertex );

                    pos += ( sizeof ( float ) * 9 );

                }

                pos = 0;
                numFaces = bin.ReadInt32 ();
                meshPolygonNum = numFaces;
                pos += sizeof ( int );
                while ( pos < polygonType * sizeof ( int ) * numFaces )
                {
                    FaceInfo polygonFace = new FaceInfo ( polygonType );        // TODO polygonType has been hard coded 
                    int vertexIndex = 0;
                    for ( int i = 0 ; i < polygonType ; i++ )
                    {
                        vertexIndex = bin.ReadInt32 ();
                        polygonFace.SetFaceVertex ( i, vertexIndex );

                    }
                    listOfPolygons.Add ( polygonFace );
                    pos += ( sizeof ( int ) * polygonType );
                }

                Debug.Log ( "Loaded DataStructure Stats : Vertex List Size:" + listOfVertices.Count + " Polygon List Size : " + listOfPolygons.Count );
            }
            fstream.Close ();
            if ( stopwatch.IsRunning )
                stopwatch.Stop ();
        }

        // Calculate Elapsed Time
        TimeSpan ts = stopwatch.Elapsed;
        Debug.Log ( "Loading Time: " + ts.Hours + " Hours " + ts.Minutes + " Mins " + ts.Seconds + " Seconds " + ts.Milliseconds + " Milliseconds " );
        /* Construct Mesh using any one of the following methods */
        StartCoroutine (instantiatePolyMesh () );
        //StartCoroutine (instantiatePointCloudMesh ());
        //StartCoroutine (instantiateSplatMesh ());
        //StartCoroutine (instantiateParticleMesh ());

        /* Enable UI Control*/
        isLoaded = true;
        CameraObject.GetComponent<CameraMovementControl> ().isMovementActive = true;
    }

    IEnumerator loadObj ( string filePath )
    {
        // Start Timer
        if ( !stopwatch.IsRunning )
            stopwatch.Start ();

        // Read file
        StreamReader sr = new StreamReader ( filePath );
        string [] buffer = sr.ReadLine ().Split ();
        numVertices = int.Parse ( buffer [ 1 ] );   // Read vertices here
        buffer = sr.ReadLine ().Split ();
        numFaces = int.Parse ( buffer [ 1 ] );      // Read number of faces here

        listOfPolygons = new List<FaceInfo> ();
        listOfVertices = new List<VertexInfo> ();
        minValue = new Vector3 ();                  // For moving the object around origin

        Vector3 positionVector = Vector3.zero;
        Vector3 normalVector = Vector3.zero;
        Color colorVector = Color.white;

        string fileStreamLine = sr.ReadLine ();

        for ( int i = 0 ; i < ( ( 2 * numVertices ) + numFaces ) ; i++ )
        {
            buffer = fileStreamLine.Split ();

            switch ( buffer [ 0 ] )
            {
                case "vn":
                    normalVector = new Vector3 ( float.Parse ( buffer [ 1 ] ) * scale, float.Parse ( buffer [ 2 ] ) * scale, float.Parse ( buffer [ 3 ] ) * scale );
                    break;

                case "v":
                    positionVector = new Vector3 ( float.Parse ( buffer [ 1 ] ) * scale, float.Parse ( buffer [ 2 ] ) * scale, float.Parse ( buffer [ 3 ] ) * scale );
                    if ( buffer.Length > 4 )
                    {
                        colorVector = new Color ( float.Parse ( buffer [ 4 ] ), float.Parse ( buffer [ 5 ] ), float.Parse ( buffer [ 6 ] ) );
                    }
                    else
                    {
                        colorVector = Color.white;
                    }

                    VertexInfo newVertex = new VertexInfo ( positionVector, normalVector, colorVector );
                    listOfVertices.Add ( newVertex );

                    /* Shift to origin */
                    calculateMin ( positionVector );

                    /* Update UI cummulatively for vertex position as well as normals*/

                    progress = i * 1.0f / ( 2 * numVertices - 1 ) * 1.0f;
                    if ( Math.Abs ( progress % Mathf.FloorToInt ( ( 2 * numVertices ) / 20 ) - 0 ) < Single.Epsilon )
                    {
                        guiString = ( ( progress / 2 ) ).ToString () + " out of " + numVertices.ToString () + " vertices read";
                        yield return null;
                    }
                    break;

                case "f":
                    FaceInfo tempObject = new FaceInfo ( polygonType ); //Empty object that stores polygonType and corresponding vertex indices

                    for ( int j = 1 ; j < buffer.Length ; j++ )
                    {
                        string [] delimiter = new string [] { "//" }; // Delimiter in obj file for faces is "//"
                        string [] resultingString = buffer [ j ].Split ( delimiter, StringSplitOptions.None );
                        int resultingIndex = int.Parse ( resultingString [ 0 ] );
                        resultingIndex--;   // Start index at one less than what is mentioned so that 1 is saved as 0 and so on
                        tempObject.SetFaceVertex ( j - 1, resultingIndex );
                    }
                    listOfPolygons.Add ( tempObject );
                    meshPolygonNum++;

                    /* Update UI */
                    progress = meshPolygonNum * 1.0f / ( numFaces - 1 ) * 1.0f;
                    if ( meshPolygonNum % Mathf.FloorToInt ( numFaces / 20 ) == 0 )
                    {
                        guiString = "Loading Mesh";
                        yield return null;
                    }

                    break;
                default:
                    Debug.Log ( "STRAY INPUT IN FILE : " + buffer [ 0 ] );
                    break;
            }
            fileStreamLine = sr.ReadLine ();
        }

        /* Close file stream*/
        sr.Close ();

        if ( stopwatch.IsRunning )
            stopwatch.Stop ();

        // Calculate Elapsed Time
        TimeSpan ts = stopwatch.Elapsed;
        Debug.Log ( "Loading Time: " + ts.Hours + " Hours " + ts.Minutes + " Mins " + ts.Seconds + " Seconds " + ts.Milliseconds + " Milliseconds " );

        Debug.Log ( "Number of Vertices :" + numVertices + " Number of Faces : " + numFaces );
        Debug.Log ( "DataStructure Stats : Vertex List Size:" + listOfVertices.Count + " Normal List Size:" + listOfVertices.Count + " Color List Size:" + listOfVertices.Count + " Polygon List Size:" + listOfPolygons.Count );

        /* Construct Mesh using any one of the following methods */
        StartCoroutine ( instantiatePointCloudMesh () );
        //StartCoroutine (instantiateSplatMesh ());
        //StartCoroutine (instantiatePolyMesh ());
        //StartCoroutine (instantiateParticleMesh ());

        /* Enable UI Control*/
        isLoaded = true;
        CameraObject.GetComponent<CameraMovementControl> ().isMovementActive = true;
    }

    IEnumerator instantiatePolyMesh ()
    {
        meshGameObject = new GameObject ( "polyMesh" );
        
        /* Instantiate Mesh here*/
        meshGroupWidth = Mathf.FloorToInt ( ( MAX_VERTICES * 1.0f ) / ( polygonType * 1.0f ) );
        meshGroupNumMax = Mathf.CeilToInt ( ( meshPolygonNum * 1.0f ) / ( meshGroupWidth * 1.0f ) );
        Debug.Log ( "meshPolygonNum: " + meshPolygonNum.ToString () + " meshGroupNumMax: " + meshGroupNumMax.ToString () + " meshGroupWidth: " + meshGroupWidth.ToString () );
        for ( meshGroupNum = 1 ; meshGroupNum < meshGroupNumMax ; meshGroupNum++ )
        {

            InstantiatePolyFace ( meshGroupNum, MAX_VERTICES, meshGroupWidth );
            progress = meshGroupNum * 1.0f / ( meshGroupNumMax ) * 1.0f;
            if ( meshGroupNum % Mathf.FloorToInt ( meshGroupNumMax / 3 ) == 0 )
            {
                guiString = meshGroupNum.ToString () + " out of " + meshGroupNumMax.ToString () + " Mesh Groups loaded";
                yield return null;
            }
        }

        int remainingPoints = ( meshPolygonNum - ( ( meshGroupNum - 1 ) * meshGroupWidth ) ) * polygonType;
        InstantiatePolyFace ( meshGroupNum, remainingPoints, Mathf.FloorToInt ( ( remainingPoints * 1.0f ) / ( polygonType * 1.0f ) ) ); // remaining point must be less than MAX_POINTS per mesh
    }

    void InstantiatePolyFace ( int meshId, int nPoints, int groupWidth )
    {
        // Create Mesh
        GameObject meshGroup = new GameObject ( "MeshPart_" + meshId );
        meshGroup.SetActive ( false );              // Deactivate Object in the scene
        meshGroup.tag = "PolygonMesh";              // Set object Tag
        meshGroup.isStatic = true;                  // Enable Static Batching
        meshGroup.AddComponent<MeshFilter> ();
        meshGroup.AddComponent<MeshRenderer> ();
        meshGroup.GetComponent<MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        Mesh mesh = new Mesh ();
        List<int> meshTriangleList = new List<int> ();
        Vector3 [] meshVertices = new Vector3 [ nPoints ];
        Vector3 [] meshNormals = new Vector3 [ nPoints ];
        Color [] meshColors = new Color [ nPoints ];
        FaceInfo tempObject = null;
        int vertexCount = 0; // Verices inserted into the mesh as well as their index
        int vertexIndex = 0;
        int iterator = 0;
        int startOfItertaion = ( 0 + ( meshId - 1 ) * meshGroupWidth );
        int limitOfIteration = ( ( meshId - 1 ) * meshGroupWidth ) + groupWidth;

        while ( vertexCount < nPoints )
        {
            for ( iterator = startOfItertaion ; iterator < limitOfIteration ; iterator++ )
            {
                tempObject = listOfPolygons [ iterator ];
                for ( int j = 0 ; j < polygonType ; j++ )
                {   // Adds vertices to point array as well as triangle array 3/4 at a time
                    if ( vertexCount > nPoints )
                        Debug.Log ( "MAX_VERTICES/LIMIT_POINTS EXCEEDED" );
                    vertexIndex = tempObject.ReturnFacesArrayVal ( j );
                    meshVertices [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Position;
                    meshNormals [ vertexCount ] = ( Vector3 ) listOfVertices [ vertexIndex ].Normal;
                    meshColors [ vertexCount ] = ( Color ) listOfVertices [ vertexIndex ].Vcolor;
                    meshTriangleList.Add ( vertexCount );
                    vertexCount++;
                }
            }
        }

        /* Assign vertices, normals, colors and mesh triangles */
        mesh.vertices = meshVertices;
        mesh.normals = meshNormals;
        mesh.colors = meshColors;
        mesh.triangles = meshTriangleList.ToArray ();

        // UV Value not read from OBJ file
        mesh.uv = new Vector2 [ nPoints ];

        meshGroup.GetComponent<MeshFilter> ().mesh = mesh;
        meshGroup.GetComponent<Renderer> ().material = renderingMaterial;
        meshGroup.transform.parent = meshGameObject.transform;
        meshGroup.SetActive ( true );       // Activate object in the scene
        // Store Mesh
        //UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
        //UnityEditor.AssetDatabase.SaveAssets ();
        //UnityEditor.AssetDatabase.Refresh();
    }

    IEnumerator instantiatePointCloudMesh ()
    {
        /* Logic for construction of point cloud */
        meshGameObject = new GameObject ( "PoinCloudMesh" );

        pointGroupNumMax = Mathf.CeilToInt ( numVertices * 1.0f / MAX_VERTICES * 1.0f );    // Number of point groups each made up of 65535 Vertices
        Debug.Log ( "Number of Point Groups: " + pointGroupNumMax );
        for ( int i = 0 ; i < pointGroupNumMax - 1 ; i++ )
        {
            InstantiatePointMesh ( i, MAX_VERTICES );
            if ( i % 10 == 0 )
            {
                guiString = i.ToString () + " out of " + pointGroupNumMax.ToString () + " PointGroups loaded";
                yield return null;
            }
        }

        /* Last point group has been instantiated here */
        InstantiatePointMesh ( pointGroupNumMax - 1, numVertices - ( pointGroupNumMax - 1 ) * MAX_VERTICES );

        //Store PointCloud
        //UnityEditor.PrefabUtility.CreatePrefab ("Assets/Resources/pointCloudMesh" + ".prefab", pointCloudObject);
    }

    void InstantiatePointMesh ( int meshId, int nPoints )
    {
        // Create Mesh
        GameObject meshGroup = new GameObject ( "PointCloudMesh_" + meshId );
        meshGroup.SetActive ( false );                  // Deactivate object in the scene
        meshGroup.tag = "PointMesh";                    // Set object tag
        meshGroup.isStatic = true;                      // Enable Static Batching
        meshGroup.AddComponent<MeshFilter> ();
        meshGroup.AddComponent<MeshRenderer> ();
        meshGroup.GetComponent<MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        meshGroup.GetComponent<Renderer> ().material = renderingMaterial;

        Mesh mesh = new Mesh ();
        Vector3 [] vertexPos = new Vector3 [ nPoints ];
        Vector3 [] vertexNorm = new Vector3 [ nPoints ];
        int [] indicies = new int [ nPoints ];
        Color [] vertexColor = new Color [ nPoints ];

        for ( int i = 0 ; i < nPoints ; ++i )
        {
            vertexPos [ i ] = ( Vector3 ) ( listOfVertices [ meshId * MAX_VERTICES + i ].Position ) - minValue;
            indicies [ i ] = i;
            vertexNorm [ i ] = ( Vector3 ) ( listOfVertices [ meshId * MAX_VERTICES + i ].Normal );
            vertexColor [ i ] = ( Color ) listOfVertices [ meshId * MAX_VERTICES + i ].Vcolor;
        }

        /* Assign vertices, normals, colors and mesh triangles */
        mesh.vertices = vertexPos;
        mesh.colors = vertexColor;
        mesh.SetIndices ( indicies, MeshTopology.Points, 0 );
        mesh.uv = new Vector2 [ nPoints ];
        mesh.normals = vertexNorm;
        mesh.RecalculateBounds ();

        meshGroup.GetComponent<MeshFilter> ().mesh = mesh;
        meshGroup.transform.parent = meshGameObject.transform;
        meshGroup.SetActive (true);                 // Activate object in the scene
        // Store Mesh
        //UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + meshInd + ".asset");
        //UnityEditor.AssetDatabase.SaveAssets ();
        //UnityEditor.AssetDatabase.Refresh();
    }

    IEnumerator instantiateSplatMesh ()
    {
        meshGameObject = new GameObject ( "SplatMesh" );
        int index = 0, maxIndex = listOfVertices.Count;
        SerialVector3 positionVector, normalVector;
        for ( index = 0 ; index < maxIndex ; index++ )
        {
            positionVector = listOfVertices [ index ].Position;
            normalVector = listOfVertices [ index ].Normal;
            InstantiateSplat ( positionVector, normalVector, index );

            progress = index * 1.0f / ( numVertices - 1 ) * 1.0f;
            if ( index % Mathf.FloorToInt ( numVertices / 20 ) == 0 )
            {
                guiString = index.ToString () + " out of " + maxIndex.ToString () + " splats instantiated";
                yield return null;
            }
        }

        //Store PointCloud
        //UnityEditor.PrefabUtility.CreatePrefab ("Assets/Resources/pointCloudMesh" + ".prefab", meshGameObject);
    }

    void InstantiateSplat ( SerialVector3 posElement, SerialVector3 normElement, int index )
    {
        Vector3 normal = ( Vector3 ) ( normElement );
        if ( Math.Abs ( normal.magnitude - 0 ) < Single.Epsilon )
            return; // If normal at that vertex position is zero then don't instantiate splat

        GameObject quadSplat = GameObject.CreatePrimitive ( PrimitiveType.Quad );
        quadSplat.SetActive ( false );                          // Deactivate object in the scene
        quadSplat.tag = "SplatMesh";                            // Set object tag
        Destroy (quadSplat.GetComponent<MeshCollider>());// Collider is not needed
        quadSplat.isStatic = true;                       // Enable Static batching for quads
        quadSplat.GetComponent<MeshRenderer> ().material = renderingMaterial;
        quadSplat.GetComponent<MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        quadSplat.transform.position = ( Vector3 ) ( posElement );
        quadSplat.transform.rotation = Quaternion.FromToRotation ( -Vector3.forward, normal );
        quadSplat.transform.localScale = new Vector3 ( splatScale, splatScale, splatScale );
        quadSplat.transform.parent = meshGameObject.transform;
        quadSplat.SetActive (true);                         // Activate object in the scene
    }

    

    IEnumerator instantiateParticleMesh ()
    {
        /*
			To run this method, insert a gameObject into the scene named as 
			PointCloudObject. Now attach this script to that game object and
			then run the load scene. This will render the point cloud as a 
			system of particles.
		 */

        this.gameObject.GetComponent<ParticleSystem> ().loop = false;
        this.gameObject.GetComponent<ParticleSystem> ().maxParticles = 1000000;
        this.gameObject.GetComponent<ParticleSystem> ().playOnAwake = false;
        this.gameObject.GetComponent<ParticleSystem> ().startSpeed = 0;
        this.gameObject.GetComponent<ParticleSystem> ().startSize = 0.01f;
        this.gameObject.GetComponent<ParticleSystem> ().enableEmission = false;
        this.gameObject.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().receiveShadows = true;
        this.gameObject.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        ParticleSystem.Particle [] particleCloud;
        particleCloud = new ParticleSystem.Particle [ numVertices ];
        for ( int i = 0 ; i < numVertices ; i++ )
        {
            particleCloud [ i ].position = ( Vector3 ) listOfVertices [ i ].Position;
            particleCloud [ i ].color = ( Color ) listOfVertices [ i ].Vcolor;
            particleCloud [ i ].angularVelocity = 0;
            particleCloud [ i ].size = 0.01f;
            particleCloud [ i ].lifetime = 10000;
            progress = i * 1.0f / ( 2 * numVertices - 1 ) * 1.0f;
            if ( Math.Abs ( progress % Mathf.FloorToInt ( ( 2 * numVertices ) / 20 ) - 0 ) < Single.Epsilon )
            {
                guiString = ( ( progress / 2 ) ).ToString () + " out of " + numVertices.ToString () + " vertices read";
                yield return null;
            }
        }

        this.gameObject.GetComponent<ParticleSystem> ().SetParticles ( particleCloud, numVertices );
        this.gameObject.GetComponent<ParticleSystem> ().enableEmission = true;

    }

    void calculateMin ( Vector3 point )
    {
        if ( Math.Abs ( minValue.magnitude - 0 ) < Single.Epsilon )
            minValue = point;

        if ( point.x < minValue.x )
            minValue.x = point.x;
        if ( point.y < minValue.y )
            minValue.y = point.y;
        if ( point.z < minValue.z )
            minValue.z = point.z;
    }

    void printVertex ( Vector3 inputVector )
    {
        Debug.Log ( "Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")" );
    }

    void printVertex ( SerialVector3 serialInputVector )
    {
        Vector3 inputVector = ( Vector3 ) serialInputVector;
        Debug.Log ( "Vector Information: Position (" + inputVector.x + " , " + inputVector.y + " , " + inputVector.z + ")" );
    }

    void OnGUI ()
    {
        if ( !isLoaded )
        {
            GUI.BeginGroup ( new Rect ( Screen.width / 2 - 100, Screen.height / 2, 400.0f, 30 ) );
            GUI.Box ( new Rect ( 0, 0, 400.0f, 30.0f ), guiString );
            GUI.Box ( new Rect ( 0, 0, progress * 400.0f, 30 ), "" );
            GUI.EndGroup ();
        }
    }


}
