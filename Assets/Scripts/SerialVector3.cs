﻿using System.Collections;
using UnityEngine;

/*
	This class serializes the Vector3 data type
 */

[System.Serializable]
public class SerialVector3
{

    private float _x;
    private float _y;
    private float _z;
    //	private float _w;

    public SerialVector3 ()
    {
        this._x = 0;
        this._y = 0;
        this._z = 0;
        //		this._w = 1;
    }
    public SerialVector3 ( Vector3 vec3 )
    {
        this._x = vec3.x;
        this._y = vec3.y;
        this._z = vec3.z;
        //		this._w = 1;
    }

    public static implicit operator SerialVector3 ( Vector3 vec3 )
    {
        return new SerialVector3 ( vec3 );
    }
    public static explicit operator Vector3 ( SerialVector3 wb_vec3 )
    {
        return new Vector3 ( wb_vec3._x, wb_vec3._y, wb_vec3._z );
    }
}
