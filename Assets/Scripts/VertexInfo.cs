﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class VertexInfo
{
    private Vector3 _position;
    private Vector3 _normal;
    private Color _color;

    public Vector3 Position
    {
        get { return _position; }
        set { this._position = value; }
    }
    public Vector3 Normal
    {
        get { return _normal; }
        set { this._normal = value; }
    }
    public Color Vcolor
    {
        get { return _color; }
        set { this._color = value; }
    }

    public VertexInfo ()
    {
        this._position = new Vector3 ();
        this._normal = new Vector3 ();
        this._color = new Color ();
    }

    public VertexInfo ( Vector3 pos, Vector3 norm, Color col )
    {
        this._position = new Vector3 ( pos.x, pos.y, pos.z );
        this._normal = new Vector3 ( norm.x, norm.y, norm.z );
        this._color = new Color ( col.r, col.g, col.b );
    }


    /*
	private SerialVector3 _position;
	private SerialVector3 _normal;
	private SerialColor _color;

	public SerialVector3 Position {
		get { return _position;}
		set { _position = value;}
	}
	public SerialVector3 Normal {
		get { return _normal;}
		set { _normal = value;}
	}
	public SerialColor Vcolor {
		get { return _color;}
		set { _color = value;}
	}

	public VertexInfo ()
	{
		this._position = new SerialVector3 ();
		this._normal = new SerialVector3 ();
		this._color = new SerialColor ();
	}

	public VertexInfo (Vector3 pos, Vector3 norm, Color col)
	{
		this._position = new SerialVector3 (pos);
		this._normal = new SerialVector3 (norm);
		this._color = new SerialColor (col);
	}
	*/
}
