﻿// Copyright (c) 2014-2015 StagPoint Consulting
		
using System;

using UnityEngine;

namespace StagPoint.SpatialDatabase
{

	/// <summary>
	/// Defines the required implementation for tokens that are returned 
	/// by an ISpatialPartitioner implementation, which are used to update an 
	/// object's position within that implementation.
	/// </summary>
	public interface ISpatialToken
	{

		/// <summary>
		/// Returns the item's axis-aligned bounding box
		/// </summary>
		Bounds Bounds { get; }

		/// <summary>
		/// Remove the item from tracking 
		/// </summary>
		void Remove();

		/// <summary>
		/// Update the item's tracking information
		/// </summary>
		void Update( Bounds newBounds );

	}

	/// <summary>
	/// Defines the abstract interface that will be implemented by all
	/// spatial database types, such as the Octree and UniformGrid classes.
	/// </summary>
	interface ISpatialDatabase
	{

		Bounds EnclosedArea { get; }

		ISpatialToken AddItem( object item, Bounds bounds );
		void Reset();
		void ExecuteQuery<T>( SpacialDatabaseQuery<T> query ) where T : class;
		
#if UNITY_EDITOR
		void OnDrawGizmos();
#endif

	}

}
