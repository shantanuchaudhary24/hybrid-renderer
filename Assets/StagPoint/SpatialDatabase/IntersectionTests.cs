﻿// Copyright (c) 2014-2015 StagPoint Consulting
		
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace StagPoint.SpatialDatabase
{

	/// <summary>
	/// Provides information about whether an intersection exists between two
	/// geometric primitives, as well as whether the objects simply intersect
	/// or whether the first object fully contains the second
	/// </summary>
	public enum IntersectionType
	{
		/// <summary>
		/// No intersection exists between the tested objects
		/// </summary>
		None,
		/// <summary>
		/// The first object intersects with, but does not fully contain, the second object
		/// </summary>
		Intersects,
		/// <summary>
		/// The first object fully contains the second object
		/// </summary>
		Contains
	}

	/// <summary>
	/// Provides a set of optimized functions for performing common intersection 
	/// tests between various typeof of geometric primitives.
	/// </summary>
	public class IntersectionTests
	{

		/// <summary>
		/// Returns true if the ray intersects the axis-aligned bounding box
		/// </summary>
		/// <param name="ray">The ray to test for intersection</param>
		/// <param name="bounds">The axis-aligned bounding box to check for intersection</param>
		/// <returns></returns>
		public static bool Intersects( ref Ray ray, ref Bounds bounds )
		{

			var endPoint = ray.origin + ray.direction * float.MaxValue * 0.5f;
			var startPoint = ray.origin;

			return Intersects( ref startPoint, ref endPoint, ref bounds );

		}

		/// <summary>
		/// Returns true if the line segment intersects the axis-aligned bounding box
		/// </summary>
		/// <param name="start">The start point of the line segment</param>
		/// <param name="end">The end point of the line segment</param>
		/// <param name="bounds">The axis-aligned bounding box to check for intersection</param>
		/// <returns></returns>
		public static bool Intersects( ref Vector3 start, ref Vector3 end, ref Bounds bounds )
		{

			var e = bounds.max - bounds.min;
			var d = end - start;
			var m = start + end - bounds.min - bounds.max;

			// Try world coordinate axes as separating axes
			float adx = Math.Abs( d.x );
			if( Math.Abs( m.x ) > e.x + adx )
				return false;
			float ady = Math.Abs( d.y );
			if( Math.Abs( m.y ) > e.y + ady )
				return false;
			float adz = Math.Abs( d.z );
			if( Math.Abs( m.z ) > e.z + adz )
				return false;

			// Add in an epsilon term to counteract arithmetic errors when segment is
			// (near) parallel to a coordinate axis (see text for detail)
			adx += float.Epsilon;
			ady += float.Epsilon;
			adz += float.Epsilon;

			// Try cross products of segment direction vector with coordinate axes
			if( Math.Abs( m.y * d.z - m.z * d.y ) > e.y * adz + e.z * ady )
				return false;
			if( Math.Abs( m.z * d.x - m.x * d.z ) > e.x * adz + e.z * adx )
				return false;
			if( Math.Abs( m.x * d.y - m.y * d.x ) > e.x * ady + e.y * adx )
				return false;

			// No separating axis found; segment must be overlapping AABB
			return true;

		}

		/// <summary>
		/// Returns TRUE if the axis-aligned bounding box and the sphere overlap
		/// </summary>
		public static bool Intersects( ref Bounds box, ref Vector3 center, float radius )
		{

			var closestPointInAabb = Vector3.Min( Vector3.Max( center, box.min ), box.max );
			var distanceSquared = ( closestPointInAabb - center ).sqrMagnitude;

			return distanceSquared <= radius * radius;

		}

		/// <summary>
		/// Returns the type of intersection (if any) between an axis-aligned bounding box and a sphere
		/// </summary>
		public static IntersectionType GetIntersectionType( ref Bounds box, ref Vector3 center, float radius )
		{

			var closestPointInBox = Vector3.Min( Vector3.Max( center, box.min ), box.max );
			var distanceSquared = ( closestPointInBox - center ).sqrMagnitude;

			if( distanceSquared <= ( radius * radius ) )
			{

				var c = closestPointInBox - box.center;
				var r = box.extents;
				var x = Math.Abs( c.x ) <= ( r.x - radius );
				var y = Math.Abs( c.y ) <= ( r.y - radius );
				var z = Math.Abs( c.z ) <= ( r.z - radius );

				if( x & y & z )
				{
					return IntersectionType.Contains;
				}

				return IntersectionType.Intersects;

			}

			return IntersectionType.None;

		}

		/// <summary>
		/// Returns the type of intersection (if any) between a sphere and an axis-aligned bounding box.
		/// </summary>
		public static IntersectionType GetIntersectionType( ref Vector3 center, float radius, ref Bounds box )
		{

			var closestPointInBox = Vector3.Min( Vector3.Max( center, box.min ), box.max );
			var distanceSquared = ( closestPointInBox - center ).sqrMagnitude;

			if( distanceSquared <= ( radius * radius ) )
			{

				var c = ( box.center - center ).magnitude;
				var r = ( box.max - box.center ).magnitude;

				if( c <= radius - r )
				{
					return IntersectionType.Contains;
				}

				return IntersectionType.Intersects;

			}

			return IntersectionType.None;

		}

		/// <summary>
		/// Returns TRUE if both axis-aligned bounding boxes overlap. This function
		/// is analogous to Bounds.Intersects(), but seems to perform better
		/// </summary>
		public static bool Intersects( ref Bounds lhs, ref Bounds rhs )
		{

			var c = lhs.center - rhs.center;
			var r = lhs.extents + rhs.extents;

			var x = Math.Abs( c.x ) <= r.x;
			var y = Math.Abs( c.y ) <= r.y;
			var z = Math.Abs( c.z ) <= r.z;

			return x & y & z;

		}

		/// <summary>
		/// Returns the type of intersection (if any) between two axis-aligned bounding boxes
		/// </summary>
		public static IntersectionType GetIntersectionType( ref Bounds lhs, ref Bounds rhs )
		{

			var c = lhs.center - rhs.center;
			var r = lhs.extents + rhs.extents;

			var x = Math.Abs( c.x ) <= r.x;
			var y = Math.Abs( c.y ) <= r.y;
			var z = Math.Abs( c.z ) <= r.z;

			if( x & y & z )
			{

				r = lhs.extents - rhs.extents;
				x = Math.Abs( c.x ) <= r.x;
				y = Math.Abs( c.y ) <= r.y;
				z = Math.Abs( c.z ) <= r.z;

				if( x & y & z )
				{
					return IntersectionType.Contains;
				}

				return IntersectionType.Intersects;

			}

			return IntersectionType.None;

		}

		/// <summary>
		/// Returns the type of intersection (if any) between an axis-aligned bounding box
		/// and a plane-bounded volume.
		/// </summary>
		public static IntersectionType GetIntersectionType( Plane[] planes, ref Bounds bounds )
		{

			if( planes == null )
			{
				throw new ArgumentNullException( "planes" );
			}

			var center = bounds.center;
			var extents = bounds.extents;

			var intersecting = false;

			var planeCount = planes.Length;
			for( int i = 0; i < planeCount; i++ )
			{

				var plane = planes[ i ];
				var planeNormal = plane.normal;

				// Compute the projection interval radius of box b onto L(t) = b.c + t * p.n
				var r =
					extents.x * Math.Abs( planeNormal.x ) +
					extents.y * Math.Abs( planeNormal.y ) +
					extents.z * Math.Abs( planeNormal.z );

				// Compute distance of box center from plane
				float distance = Vector3.Dot( planeNormal, center ) + plane.distance;

				if( distance < -r )
				{
					// If the AABB lies behind *any* of the planes, there
					// is no point in continuing with the rest of the test
					return IntersectionType.None;
				}

				// Intersection occurs when distance falls within [-r,+r] interval
				intersecting |= ( Math.Abs( distance ) <= r );

			}

			if( intersecting )
			{
				return IntersectionType.Intersects;
			}

			return IntersectionType.Contains;

		}

	}

}
