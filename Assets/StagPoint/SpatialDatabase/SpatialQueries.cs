﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using StagPoint.Core;

namespace StagPoint.SpatialDatabase
{

	/// <summary>
	/// This base class describes the required functionality for any query executed 
	/// by the SpatialDatabase or Octree classes
	/// </summary>
	/// <typeparam name="T">The type of the item being searched for</typeparam>
	public abstract class SpacialDatabaseQuery<T> : IDisposable where T : class
	{

		#region Delegate definitions 

		public delegate bool IntersectsItemCallback<TItem> (ref Bounds bounds,TItem item);

		#endregion 

		#region Public fields

		/// <summary>
		/// If this callback is assigned, it will be called instead of the IntersectsItem()
		/// function, allowing the query consumer to override the intersection logic.
		/// </summary>
		public IntersectsItemCallback<T> IntersectsItemOverride;

		#endregion 

		#region Required functions

		/// <summary>
		/// Returns the type of intersection (if any) that exists between this query's 
		/// search area and the supplied axis-aligned bounding box. 
		/// </summary>
		/// <param name="bounds">The axis-aligned bounding box to test for intersection</param>
		/// <returns>Returns IntersectionType.None if there is no intersection between the query's 
		/// search area and the supplied bounding box, InterSectionType.Intersects if the bounding 
		/// box overlaps the search area but is not fully contained by it, and IntersectionType.Contains
		/// if the search area fully contains the supplied bounding box.</returns>
		public abstract IntersectionType GetVolumeIntersection (ref Bounds bounds);

		/// <summary>
		/// Returns TRUE if the item's bounds intersect the search area. This function is called by 
		/// the SpatialDatabase instance when determining whether to include an individual item in the
		/// results, and is separate from the GetIntersectionType method because it is not necessary
		/// to determine whether the item's axis-aligned bounding box is fully contained within the 
		/// search area, which provides an opportunity in many cases to use a computationally cheaper 
		/// intersection test.
		/// </summary>
		/// <param name="bounds">The individual item's axis-aligned bounding box</param>
		public virtual bool IntersectsItem (ref Bounds bounds)
		{
			return GetVolumeIntersection (ref bounds) != IntersectionType.None;
		}

		/// <summary>
		/// Must be overridden by the query implementation to allow processing of individual items 
		/// that are returned by the query
		/// </summary>
		/// <param name="result"></param>
		public abstract void ProcessResult (T result);

		#endregion

		#region Functions that can be overridden

		/// <summary>
		/// This function is called by SpatialDatabase just before the query is executed, and provides
		/// the query an opportunity to initialize any internal state that is needed. For instance, this
		/// can provide an opportunity to pre-calculate any values that will be used during the query.
		/// </summary>
		public virtual void Prepare ()
		{
			// Base implementation does nothing
		}

		/// <summary>
		/// This function is called by the SpatialDatabase just after the query has finished executing,
		/// and just before the query is returned to the caller.
		/// </summary>
		public virtual void Finish ()
		{
			// Base implementation does nothing
		}

		#endregion

		#region IDisposable Implementation

		/// <summary>
		/// Prepares the query for re-use and returns it back to the object pool
		/// </summary>
		public abstract void Dispose ();

		#endregion

	}

	/// <summary>
	/// Used to perform a linecast or raycast query in a SpatialDatabase instance 
	/// that will find all items that are intersected by the given line.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class LineSegmentQuery<T> : SpacialDatabaseQuery<T> where T : class
	{

		#region Object Pooling

		private static ListEx<object> pool = new ListEx<object> ();

		/// <summary>
		/// Returns an instance of this class from the object pool if the object pool is
		/// not empty, or a new instance.
		/// </summary>
		/// <param name="planes">The planes that enclose the desired area to search</param>
		public static LineSegmentQuery<T> Obtain (Vector3 lineStart, Vector3 lineEnd)
		{

			LineSegmentQuery<T> instance = null;

			lock (pool) {
				if (pool.Count > 0) {
					instance = (LineSegmentQuery<T>)pool.Pop ();
				} else {
					instance = new LineSegmentQuery<T> ();
				}
			}

			instance.results = ListEx<T>.Obtain ();
			instance.lineStart = lineStart;
			instance.lineEnd = lineEnd;

			return instance;

		}

		/// <summary>
		/// Returns the object back to the object pool, ready to be re-used
		/// </summary>
		public override void Dispose ()
		{

			lock (pool) {

				this.results.Release ();
				this.results = null;

				pool.Add (this);

			}

		}

		private LineSegmentQuery ()
		{
			// Private constructor disallows new() from anywhere
			// but the Obtain() method
		}

		#endregion

		#region Public properties  

		/// <summary>
		/// Returns a list containing all results of the executed query
		/// </summary>
		public IList<T> Results { get { return this.results; } }

		/// <summary>
		/// Returns the start point of the line segment used in the query
		/// </summary>
		public Vector3 LineStart { get { return this.lineStart; } }

		/// <summary>
		/// Returns the end point of the line segment used in the query
		/// </summary>
		public Vector3 LineEnd { get { return this.lineEnd; } }

		#endregion 

		#region Private fields 

		private Vector3 lineStart;
		private Vector3 lineEnd;

		private ListEx<T> results = null;

		#endregion 

		#region SpacialQuery<T> implementation 

		public override IntersectionType GetVolumeIntersection (ref Bounds bounds)
		{
			return IntersectionTests.Intersects (ref this.lineStart, ref this.lineEnd, ref bounds) 
				? IntersectionType.Intersects 
				: IntersectionType.None;
		}

		public override void ProcessResult (T result)
		{
			this.results.Add (result);
		}

		#endregion 

	}

	/// <summary>
	/// Used to perform a query in a SpatialDatabase instance that will find all 
	/// items that are contained within or intersection the area that is enclosed
	/// the the set of planes in the plane-bounded volume. For instance, this query
	/// could be used as a frustum test if the frustum has been defined as a set 
	/// of planes enclosing the viewable area.
	/// </summary>
	public class PlaneBoundedVolumeQuery<T> : SpacialDatabaseQuery<T> where T : class
	{

		#region Object Pooling

		private static ListEx<object> pool = new ListEx<object> ();

		/// <summary>
		/// Returns an instance of this class from the object pool if the object pool is
		/// not empty, or a new instance.
		/// </summary>
		/// <param name="planes">The planes that enclose the desired area to search</param>
		public static PlaneBoundedVolumeQuery<T> Obtain (Plane[] planes)
		{

			PlaneBoundedVolumeQuery<T> instance = null;

			lock (pool) {
				if (pool.Count > 0) {
					instance = (PlaneBoundedVolumeQuery<T>)pool.Pop ();
				} else {
					instance = new PlaneBoundedVolumeQuery<T> ();
				}
			}

			instance.results = ListEx<T>.Obtain ();
			instance.planes = planes;

			return instance;

		}

		/// <summary>
		/// Returns the object back to the object pool, ready to be re-used
		/// </summary>
		public override void Dispose ()
		{

			lock (pool) {

				this.results.Release ();
				this.results = null;

				pool.Add (this);

			}

		}

		private PlaneBoundedVolumeQuery ()
		{
			// Private constructor disallows new() from anywhere
			// but the Obtain() method
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Returns the list of planes that enclose the area to be searched
		/// </summary>
		public IList<Plane> Planes { get { return this.planes; } }

		/// <summary>
		/// Returns a list containing all results of the executed query
		/// </summary>
		public IList<T> Results { get { return this.results; } }

		#endregion

		#region Private fields

		private ListEx<T> results = null;

		private Plane[] planes = null;

		#endregion

		#region Query<T> implementation

		/// <summary>
		/// Returns the type of intersection (if any) that exists between this query's 
		/// search area and the supplied axis-aligned bounding box
		/// </summary>
		/// <param name="bounds">The axis-aligned bounding box to test for intersection</param>
		/// <returns>Returns IntersectionType.None if there is no intersection between the query's 
		/// search area and the supplied bounding box, InterSectionType.Intersects if the bounding 
		/// box overlaps the search area but is not fully contained by it, and IntersectionType.Contains
		/// if the search area fully contains the supplied bounding box.</returns>
		public override IntersectionType GetVolumeIntersection (ref Bounds bounds)
		{
			return IntersectionTests.GetIntersectionType (this.planes, ref bounds);
		}

		/// <summary>
		/// Adds the supplied object to the list of results returned by the query
		/// </summary>
		/// <param name="result">The item to add to the list of results.</param>
		public override void ProcessResult (T result)
		{
			this.results.Add (result);
		}

		#endregion

	}

	/// <summary>
	/// Used to perform a query on the SpatialDatabase instance that will find all 
	/// items that are contained within or intersecting the area that is enclosed 
	/// by the specified axis-aligned bounding box.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class BoundsQuery<T> : SpacialDatabaseQuery<T> where T : class
	{

		#region Object Pooling

		private static ListEx<object> pool = new ListEx<object> ();

		/// <summary>
		/// Returns an instance of this class from the object pool if the object pool is
		/// not empty, or a new instance.
		/// </summary>
		/// <param name="bounds">The axis-aligned bounding box enclosing the area to search</param>
		public static BoundsQuery<T> Obtain (Bounds bounds)
		{

			BoundsQuery<T> instance = null;

			lock (pool) {
				if (pool.Count > 0) {
					instance = (BoundsQuery<T>)pool.Pop ();
				} else {
					instance = new BoundsQuery<T> ();
				}
			}

			instance.results = ListEx<T>.Obtain ();
			instance.bounds = bounds;

			return instance;

		}

		/// <summary>
		/// Returns the object back to the object pool, ready to be re-used
		/// </summary>
		public override void Dispose ()
		{

			lock (pool) {

#if UNITY_EDITOR
				if (pool.Contains (this)) {
					throw new Exception ("Dispose() called on an already disposed object");
				}
#endif

				this.results.Release ();
				this.results = null;

				pool.Add (this);

			}

		}

		#endregion

		#region Public properties

		/// <summary>
		/// The axis-aligned bounding box that encloses the area to be searched
		/// </summary>
		public Bounds Bounds { get { return this.bounds; } }

		/// <summary>
		/// Returns a list containing all results of the executed query
		/// </summary>
		public IList<T> Results { get { return this.results; } }

		#endregion

		#region Private fields

		private ListEx<T> results = new ListEx<T> ();

		private Bounds bounds;

		#endregion

		#region Query<T> implementation

		/// <summary>
		/// Returns the type of intersection (if any) that exists between this query's 
		/// search area and the supplied axis-aligned bounding box
		/// </summary>
		/// <param name="bounds">The axis-aligned bounding box to test for intersection</param>
		/// <returns>Returns IntersectionType.None if there is no intersection between the query's 
		/// search area and the supplied bounding box, InterSectionType.Intersects if the bounding 
		/// box overlaps the search area but is not fully contained by it, and IntersectionType.Contains
		/// if the search area fully contains the supplied bounding box.</returns>
		public override IntersectionType GetVolumeIntersection (ref Bounds bounds)
		{
			return IntersectionTests.GetIntersectionType (ref this.bounds, ref bounds);
		}

		/// <summary>
		/// Returns TRUE if the item's bounds intersect the search area. This function is called by 
		/// the SpatialDatabase instance when determining whether to include an individual item in the
		/// results, and is separate from the GetIntersectionType method because it is not necessary
		/// to determine whether the item's axis-aligned bounding box is fully contained within the 
		/// search area, which provides an opportunity in many cases to use a computationally cheaper 
		/// intersection test.
		/// </summary>
		/// <param name="bounds">The individual item's axis-aligned bounding box</param>
		public override bool IntersectsItem (ref Bounds bounds)
		{
			return IntersectionTests.Intersects (ref this.bounds, ref bounds);
		}

		/// <summary>
		/// Adds the supplied object to the list of results returned by the query
		/// </summary>
		/// <param name="result">The item to add to the list of results.</param>
		public override void ProcessResult (T result)
		{
			this.results.Add (result);
		}

		#endregion

	}

	/// <summary>
	/// Used to perform a proximity query on the SpatialDatabase instance. This class
	/// will return all items that are determined to be within the specified radius 
	/// of the given location.
	/// </summary>
	/// <typeparam name="T">The type of item to search for</typeparam>
	public class ProximityQuery<T> : SpacialDatabaseQuery<T> where T : class
	{

		#region Object Pooling

		private static ListEx<object> pool = new ListEx<object> ();

		/// <summary>
		/// Returns an instance of this class from the object pool if the object pool is
		/// not empty, or a new instance.
		/// </summary>
		/// <param name="origin">The center of the area to be searched</param>
		/// <param name="radius">The maximum distance to search</param>
		public static ProximityQuery<T> Obtain (Vector3 origin, float radius)
		{

			ProximityQuery<T> instance = null;

			lock (pool) {
				if (pool.Count > 0) {
					instance = (ProximityQuery<T>)pool.Pop ();
				} else {
					instance = new ProximityQuery<T> ();
				}
			}

			instance.results = ListEx<T>.Obtain ();
			instance.origin = origin;
			instance.radius = radius;

			return instance;

		}

		/// <summary>
		/// Returns the object back to the object pool, ready to be re-used
		/// </summary>
		public override void Dispose ()
		{

			lock (pool) {

#if UNITY_EDITOR
				if (pool.Contains (this)) {
					throw new Exception ("Dispose() called on an already disposed object");
				}
#endif

				this.results.Release ();
				this.results = null;

				pool.Add (this);

			}

		}

		#endregion

		#region Public properties

		/// <summary>
		/// Returns the center location for the proximity query
		/// </summary>
		public Vector3 Origin { get { return this.origin; } }

		/// <summary>
		/// Returns the maximum distance for the proximity query
		/// </summary>
		public float Radius { get { return this.radius; } }

		/// <summary>
		/// Returns a list containing all results of the executed query
		/// </summary>
		public IList<T> Results { get { return this.results; } }

		#endregion

		#region Private fields

		private ListEx<T> results = new ListEx<T> ();

		private Vector3 origin;
		private float radius;

		#endregion

		#region Query<T> implementation

		/// <summary>
		/// Returns the type of intersection (if any) that exists between this query's 
		/// search area and the supplied axis-aligned bounding box
		/// </summary>
		/// <param name="bounds">The axis-aligned bounding box to test for intersection</param>
		/// <returns>Returns IntersectionType.None if there is no intersection between the query's 
		/// search area and the supplied bounding box, InterSectionType.Intersects if the bounding 
		/// box overlaps the search area but is not fully contained by it, and IntersectionType.Contains
		/// if the search area fully contains the supplied bounding box.</returns>
		public override IntersectionType GetVolumeIntersection (ref Bounds bounds)
		{
			return IntersectionTests.GetIntersectionType (ref bounds, ref this.origin, this.radius);
		}

		/// <summary>
		/// Returns TRUE if the item's bounds intersect the search area. This function is called by 
		/// the SpatialDatabase instance when determining whether to include an individual item in the
		/// results, and is separate from the GetIntersectionType method because it is not necessary
		/// to determine whether the item's axis-aligned bounding box is fully contained within the 
		/// search area, which provides an opportunity in many cases to use a computationally cheaper 
		/// intersection test.
		/// </summary>
		/// <param name="bounds">The individual item's axis-aligned bounding box</param>
		public override bool IntersectsItem (ref Bounds bounds)
		{
			return IntersectionTests.Intersects (ref bounds, ref this.origin, this.radius);
		}

		/// <summary>
		/// Adds the supplied object to the list of results returned by the query
		/// </summary>
		/// <param name="result">The item to add to the list of results.</param>
		public override void ProcessResult (T result)
		{
			this.results.Add (result);
		}

		#endregion

	}

}
