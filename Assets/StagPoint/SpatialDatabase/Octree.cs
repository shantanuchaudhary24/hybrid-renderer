﻿// Copyright (c) 2014-2015 StagPoint Consulting

using System;

using UnityEngine;

using StagPoint.Core;

namespace StagPoint.SpatialDatabase
{

	/// <summary>
	/// Implements a basic Octree, which subdivides a given region of space and 
	/// allows for fast querying of objects which intersect a given area. This 
	/// is useful for proximity queries, frustum culling, raycasting, etc.
	/// </summary>
	public class Octree : ISpatialDatabase
	{

		#region Public properties

		/// <summary>
		/// Returns the axis-aligned bounding box that encloses the entire area
		/// that this Octree instance represents.
		/// </summary>
		public Bounds EnclosedArea { get; private set; }

		#endregion

		#region Private runtime variables

		private int splitDepth = 4;
		private Node root = null;

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new instance of the Octree classw with the specified Bounds
		/// </summary>
		/// <param name="bounds">The area enclosed by this Octree</param>
		public Octree (Bounds bounds)
		{

			this.EnclosedArea = bounds;

			this.root = new Node () { bounds = bounds };

			for (int i = 0; i < splitDepth; i++) {
				root.Split ();
			}

		}

		#endregion

		#region Public functions

		/// <summary>
		/// Adds a new item to be tracked by the spatial database and returns a token that
		/// may be used to update that item's Bounds.
		/// </summary>
		/// <param name="item">The item to be tracked</param>
		/// <param name="bounds">The axis-aligned bounding box enclosing the item</param>
		/// <returns>Returns an ITrackingInfo reference that may be used to update the 
		/// item's position or bounding box in the spatial database.</returns>
		public ISpatialToken AddItem (object item, Bounds bounds)
		{

			var info = new TrackingInfo ()
			{
				item = item,
				bounds = bounds,
				tree = this
			};

			root.AddItem (info);

			return info;

		}

		/// <summary>
		/// Returns a BoundsQuery instance containing the results of searching for all items of the 
		/// given type that are contained within the specified axis-aligned bounding box.
		/// </summary>
		/// <typeparam name="T">The type of the item being searched for</typeparam>
		/// <param name="bounds">The axis-aligned bounding box of the area to search</param>
		/// <returns></returns>
		public BoundsQuery<T> ExecuteQuery<T> (Bounds bounds) where T : class
		{

			var query = BoundsQuery<T>.Obtain (bounds);

			ExecuteQuery<T> (query);

			return query;

		}

		/// <summary>
		/// Used to perform a query in an Octree instance that will find all 
		/// items that are contained within or intersecting the area that is enclosed
		/// the the set of planes in the plane-bounded volume. For instance, this query
		/// could be used as a frustum test if the frustum can be defined as a set 
		/// of planes enclosing the viewable area.
		/// </summary>
		/// <typeparam name="T">The type of the item being searched for</typeparam>
		/// <param name="planes">The set of planes that encloses the area to be searched</param>
		public PlaneBoundedVolumeQuery<T> ExecuteQuery<T> (Plane[] planes) where T : class
		{

			var query = PlaneBoundedVolumeQuery<T>.Obtain (planes);

			ExecuteQuery<T> (query);

			return query;

		}

		/// <summary>
		/// Returns a LineSegmentQuery instance containing the results of searching for 
		/// all items of the given type that intersect the given line segment.
		/// </summary>
		/// <typeparam name="T">The type of the item being searched for</typeparam>
		/// <param name="start">The start point of the line segment</param>
		/// <param name="end">The end point of the line segment</param>
		public LineSegmentQuery<T> ExecuteQuery<T> (Vector3 start, Vector3 end) where T : class
		{

			var query = LineSegmentQuery<T>.Obtain (start, end);

			ExecuteQuery<T> (query);

			return query;

		}

		/// <summary>
		/// Returns a ProximityQuery instance containing the results of searching for all items of the 
		/// given type within range of the specified position.
		/// </summary>
		/// <typeparam name="T">The type of the item being searched for</typeparam>
		/// <param name="position">The position around which to search for items</param>
		/// <param name="radius">How far from the given position to search</param>
		/// <returns></returns>
		public ProximityQuery<T> ExecuteQuery<T> (Vector3 position, float radius) where T : class
		{

			var query = ProximityQuery<T>.Obtain (position, radius);

			ExecuteQuery<T> (query);

			return query;

		}

		/// <summary>
		/// Executes the given query
		/// </summary>
		public void ExecuteQuery<T> (SpacialDatabaseQuery<T> query) where T : class
		{

			query.Prepare ();

			root.ExecuteQuery<T> (query);

			query.Finish ();

		}

		public void Reset ()
		{
			initializeNodes ();
		}

		#endregion

		#region Private utility functions

		private void initializeNodes ()
		{

			this.root = new Node () { bounds = this.EnclosedArea };

			for (int i = 0; i < splitDepth; i++) {
				root.Split ();
			}

		}

		#endregion

		#region Private nested types

		private class Node
		{

			#region Public fields

			internal Bounds bounds;

			#endregion

			#region Public properties

			public Node Parent {
				get { return this.parent; }
			}

			public bool IsEmpty {
				get { return !this.hasStoredItems; }
			}

			public bool IsLeafNode {
				get { return this.childNodes == null; }
			}

			#endregion

			#region Private runtime variables

			private Node parent;
			private Node[] childNodes;

			private ListEx<TrackingInfo> items = new ListEx<TrackingInfo> ();
			private bool hasStoredItems = false;

			#endregion

			#region Public properties

			public int ItemCount {
				get {
					return items.Count;
				}
			}

			#endregion

			#region Public methods

#if UNITY_EDITOR

			internal void OnDrawGizmos ()
			{

				if (IsEmpty)
					return;

				var color = (items.Count > 0) ? Color.red : Color.grey;

				if (items.Count > 0) {
					Gizmos.color = color * new Color (1, 1, 1, 0.1f);
					Gizmos.DrawCube (bounds.center, bounds.size);
				}

				Gizmos.color = color;
				Gizmos.DrawWireCube (bounds.center, bounds.size);

				if (this.childNodes != null) {

					for (int i = 0; i < 8; i++) {
						childNodes [i].OnDrawGizmos ();
					}

				}

			}

#endif

			internal void ExecuteQuery<T> (SpacialDatabaseQuery<T> query) where T : class
			{

				// Have the query process all items (if any) contained by this node.
				processIntersectingItems (query);

				if (this.childNodes == null)
					return;

				for (int i = 0; i < 8; i++) {

					var child = childNodes [i];

					// Skip any node that does not have stored items. Note that this field is
					// recursive, so if hasStoredItems is false we know that none of the 
					// descendent nodes contains items either.
					if (!child.hasStoredItems)
						continue;

					// Determine the type of intersection that exists between the 
					// query volume and the child node's AABB
					var intersectionType = query.GetVolumeIntersection (ref child.bounds);

					switch (intersectionType) {
					case IntersectionType.Intersects:
							// This child node intersects with the query volume, so continue 
							// processing recursively.
						child.ExecuteQuery (query);
						break;
					case IntersectionType.Contains:
							// This child node is fully enclosed by the query volume, so
							// there is no longer any need to continue testing the node
							// bounds. Simply process all contained items recursively.
						child.processFullyContainedItems (query);
						break;
					}

				}

			}

			internal void AddItem (TrackingInfo item)
			{

				// The hasStoredItems field tracks whether this node or any descendent node contains items. 
				this.hasStoredItems = true;

				if (this.childNodes != null) {

					var itemCenter = item.bounds.center;
					var nodeCenter = this.bounds.center;

					var octant = 0;
					if (itemCenter.x >= nodeCenter.x)
						octant |= 4;
					if (itemCenter.y >= nodeCenter.y)
						octant |= 2;
					if (itemCenter.z >= nodeCenter.z)
						octant |= 1;

					var child = childNodes [octant];
					var test = IntersectionTests.GetIntersectionType (ref child.bounds, ref item.bounds);

					if (test == IntersectionType.Contains) {
						child.AddItem (item);
						return;
					}

				}

				item.node = this;
				this.items.Add (item);

			}

			internal void Split ()
			{

				if (childNodes != null) {
					for (int i = 0; i < 8; i++) {
						childNodes [i].Split ();
					}
					return;
				}

				Vector3 halfDimension = bounds.extents;

				childNodes = new Node[ 8 ];

				for (int i = 0; i < 8; i++) {

					Vector3 newOrigin = bounds.center;

					newOrigin.x += halfDimension.x * ((i & 4) > 0 ? 0.5f : -0.5f);
					newOrigin.y += halfDimension.y * ((i & 2) > 0 ? 0.5f : -0.5f);
					newOrigin.z += halfDimension.z * ((i & 1) > 0 ? 0.5f : -0.5f);

					childNodes [i] = new Node ()
					{
						parent = this,
						bounds = new Bounds( newOrigin, halfDimension )
					};

				}

			}

			#endregion

			#region Private utility functions

			/// <summary>
			/// Removes an item from the node's list of stored items
			/// </summary>
			internal void RemoveItem (TrackingInfo item)
			{

				// Remove the item from the list of stored items.
				if (!items.Remove (item)) {
					throw new IndexOutOfRangeException ();
				}

				// The node may now be empty. If so, it should be flagged as such
				updateIsEmptyFlag ();

			}

			/// <summary>
			/// Updates the IsEmpty flag to reflect the current state of the Node
			/// </summary>
			private void updateIsEmptyFlag ()
			{

				// If this node has items stored directly, then set the flag to TRUE
				this.hasStoredItems = (this.items.Count > 0);

				// If the flag is not already set, set it if any child nodes have
				// the flag set.
				if (this.childNodes != null) {

					for (int i = 0; i < 8 && !this.hasStoredItems; i++) {
						this.hasStoredItems |= childNodes [i].hasStoredItems;
					}

				}

				// Walk up the tree and update all higher nodes
				if (this.parent != null) {
					this.parent.updateIsEmptyFlag ();
				}

			}


			/// <summary>
			/// Processes all stored items recursively, without checking for intersection.
			/// This function is only called when the node's bounding volume has been determined 
			/// to be wholly contained by the query volume, so by definition all child nodes 
			/// and all stored items are also contained by the query volume.
			/// </summary>
			private void processFullyContainedItems<T> (SpacialDatabaseQuery<T> query) where T : class
			{

				// Process all items directly stored in this Node
				var itemCount = items.Count;
				for (int i = 0; i < itemCount; i++) {

					var info = items [i];

					var converted = info.item as T;
					if (converted == null)
						continue;

					query.ProcessResult (converted);

				}

				if (this.childNodes == null)
					return;

				// Recursively process all items stored in child nodes
				for (int i = 0; i < 8; i++) {
					var child = childNodes [i];
					if (child.hasStoredItems) {
						child.processFullyContainedItems (query);
					}
				}

			}

			/// <summary>
			/// Iterates all items stored in this node and, for each item whose bounding volume
			/// intersects the query volume, add it to the query's list of results.
			/// </summary>
			private void processIntersectingItems<T> (SpacialDatabaseQuery<T> query) where T : class
			{

				var itemCount = items.Count;
				for (int i = 0; i < itemCount; i++) {

					var info = items [i];

					var converted = info.item as T;
					if (converted == null)
						continue;

					if (query.IntersectsItemOverride != null) {
						if (query.IntersectsItemOverride (ref info.bounds, converted)) {
							query.ProcessResult (converted);
						}
					} else if (query.IntersectsItem (ref info.bounds)) {
						query.ProcessResult (converted);
					}

				}

			}

			#endregion

		}

		private class TrackingInfo : ISpatialToken
		{

			internal Octree tree;
			internal Node node;
			internal object item;
			internal Bounds bounds;

			public Bounds Bounds { get { return this.bounds; } }

			#region ITrackingInfo Members

			public void Remove ()
			{
				node.RemoveItem (this);
				node = null;
				tree = null;
				item = null;
			}

			public void Update (Bounds newBounds)
			{

				if (node == null) {
					throw new Exception ("Attempted to update the spatial tracking for an object that is not being tracked");
				}

				// If the tracking item's bounds have not actually changed, then take no action
				if (this.bounds == newBounds)
					return;

				// Update the tracking item's bounds
				this.bounds = newBounds;

				// If the tracking item is still fully contained within the same leaf node, then 
				// this function should simply exit without further action.
				if (node.IsLeafNode) {
					if (IntersectionTests.GetIntersectionType (ref node.bounds, ref this.bounds) == IntersectionType.Contains) {
						return;
					}
				}

				// Remove the item from the current node's list of stored objects so that it can be 
				// stored within a different node
				node.RemoveItem (this);

				// Re-add the item from the top of the tree to find the new storage location
				tree.root.AddItem (this);

			}

			#endregion

		}

		#endregion

#if UNITY_EDITOR

		public void OnDrawGizmos ()
		{

			if (root == null) {
				return;
			}

			root.OnDrawGizmos ();

			var bounds = this.EnclosedArea;

			Gizmos.color = Color.white;
			Gizmos.DrawWireCube (bounds.center, bounds.size);

		}

#endif

	}

}
